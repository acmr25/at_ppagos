<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model
{
    protected $table = 'tCuentasDepartamentos';
    protected $fillable = [
                            'departamento_id',
                            'beneficiario',
                            'concepto',
                            'semana',
                            'justificacion',
                            'fecha_tope',
                            'nro_orden',
                            'nro_factura',
                            'observacion',
                            'monto_programado',
                            'igtf_programado',
                            'total_programado',
                            'monto_pagado',
                            'igtf_pagado',
                            'total_pagado',
                            'variacion',
                            'estatus'];
    
    public function usuario()
	    {
	    	return $this->belongsTo('App\User','departamento_id');
	    }
}
