<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use App\User;
use App\Cuenta;
use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;
use DB;
use Log;
use Exception;
use File;

use PDF;


class CuentasTesoreriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $arrayDias = array( 'Domingo', 'Lunes', 'Martes','Miercoles', 'Jueves', 'Viernes', 'Sabado');
        $editable='NO';
        $editable_especial='NO';            

        if ($arrayDias[date('w')] == 'Viernes') {
           $domingo = date('d-m-Y', strtotime('-5 day')); $domingo2 = date('d-m-Y', strtotime('+2 day'));
           $domingo3 = date('d-m-Y', strtotime('+9 day')); $domingo4 = date('d-m-Y', strtotime('+16 day'));

           $sabado = date('d-m-Y', strtotime('+1 day'));$sabado2 = date('d-m-Y', strtotime('+8 day'));
           $sabado3 = date('d-m-Y', strtotime('+15 day'));$sabado4 = date('d-m-Y', strtotime('+22 day'));
        }
        if ($arrayDias[date('w')] == 'Sabado') {
           $domingo = date('d-m-Y', strtotime('-6 day'));$domingo2 = date('d-m-Y', strtotime('+1 day'));
           $domingo3 = date('d-m-Y', strtotime('+8 day'));$domingo4 = date('d-m-Y', strtotime('+15 day'));

           $sabado = date('d-m-Y');$sabado2 = date('d-m-Y', strtotime('+7 day'));
           $sabado3 = date('d-m-Y', strtotime('+14 day'));$sabado4 = date('d-m-Y', strtotime('+21 day'));
        }
        if ($arrayDias[date('w')] == 'Domingo') {
            $editable='SI';
            $editable_especial='SI';            

           $domingo = date('d-m-Y');$domingo2 = date('d-m-Y', strtotime('+7 day'));
           $domingo3 = date('d-m-Y', strtotime('+14 day'));$domingo4 = date('d-m-Y', strtotime('+21 day'));

           $sabado = date('d-m-Y', strtotime('+6 day'));$sabado2 = date('d-m-Y', strtotime('+13 day'));
           $sabado3 = date('d-m-Y', strtotime('+20 day'));$sabado4 = date('d-m-Y', strtotime('+27 day'));
        }    
        if ($arrayDias[date('w')] == 'Lunes') {
            $editable='SI';
            $editable_especial='SI';            

           $domingo = date('d-m-Y', strtotime('-1 day'));$domingo2 = date('d-m-Y', strtotime('+6 day'));
           $domingo3 = date('d-m-Y', strtotime('+13 day'));$domingo4 = date('d-m-Y', strtotime('+20 day'));

           $sabado = date('d-m-Y', strtotime('+5 day'));$sabado2 = date('d-m-Y', strtotime('+12 day'));
           $sabado3 = date('d-m-Y', strtotime('+19 day'));$sabado4 = date('d-m-Y', strtotime('+26 day'));
        }
        if ($arrayDias[date('w')] == 'Martes') {
            $editable_especial='SI';            
           $domingo = date('d-m-Y', strtotime('-2 day'));$domingo2 = date('d-m-Y', strtotime('+5 day'));
           $domingo3 = date('d-m-Y', strtotime('+12 day'));$domingo4 = date('d-m-Y', strtotime('+19 day'));
           
           $sabado = date('d-m-Y', strtotime('+4 day'));$sabado2 = date('d-m-Y', strtotime('+11 day'));
           $sabado3 = date('d-m-Y', strtotime('+18 day'));$sabado4 = date('d-m-Y', strtotime('+25 day'));
        }
        if ($arrayDias[date('w')] == 'Miercoles') {
           $domingo = date('d-m-Y', strtotime('-3 day'));$domingo2 = date('d-m-Y', strtotime('+4 day'));
           $domingo3 = date('d-m-Y', strtotime('+11 day'));$domingo4 = date('d-m-Y', strtotime('+18 day'));

           $sabado = date('d-m-Y', strtotime('+3 day'));$sabado2 = date('d-m-Y', strtotime('+10 day'));
           $sabado3 = date('d-m-Y', strtotime('+17 day'));$sabado4 = date('d-m-Y', strtotime('+24 day'));
        }
        if ($arrayDias[date('w')] == 'Jueves') {
           $domingo = date('d-m-Y', strtotime('-4 day'));$domingo2 = date('d-m-Y', strtotime('+3 day'));
           $domingo3 = date('d-m-Y', strtotime('+10 day'));$domingo4 = date('d-m-Y', strtotime('+17 day'));

           $sabado = date('d-m-Y', strtotime('+2 day'));$sabado2 = date('d-m-Y', strtotime('+9 day'));
           $sabado3 = date('d-m-Y', strtotime('+16 day'));$sabado4 = date('d-m-Y', strtotime('+23 day'));
        }       
 
        $esta_semana= $domingo.' AL '.$sabado;
        $semana2= $domingo2.' AL '.$sabado2;
        $semana3= $domingo3.' AL '.$sabado3;
        $semana4= $domingo4.' AL '.$sabado4;

        $areas=User::orderBy('nombre')->lists('nombre','id');

        $cuentas = Cuenta::get();
        $cuentas->each(function($cuentas){
            $cuentas->usuario;
            return $cuentas;
        });
        if (Auth::user()->rol_id == 2 ) {
            return view('dashboard.Depto.tesoreria_index')->with('areas',$areas)->with('esta_semana',$esta_semana);
        }
        else{
            return view('dashboard.Depto.index')->with('cuentas',$cuentas)->with('esta_semana',$esta_semana)->with('semana2',$semana2)->with('semana3',$semana3)->with('semana4',$semana4)->with('editable',$editable)->with('editable_especial',$editable_especial);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $areas=User::where('rol_id','!=','4')->orderBy('nombre')->lists('nombre','id');

        $obeneficiarios=Cuenta::where('departamento_id', Auth::user()->id)->orderBy('beneficiario')->select('beneficiario')->pluck('beneficiario','beneficiario');

        $oconceptos=Cuenta::where('departamento_id', Auth::user()->id)->orderBy('concepto')->select('concepto')->pluck('concepto','concepto');
        
        return view('dashboard.Depto.form')->with('areas',$areas)->with('obeneficiarios',$obeneficiarios)->with('oconceptos',$oconceptos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        if (isset($request->area)) {
            $area=$request->area;
        }
        else{
            $area=Auth::user()->id;
        }

        $count=count($request->beneficiario);

        for ($i = 0; $i < $count; $i++) {

            $cuenta= new Cuenta;
            $cuenta->departamento_id=$area;
            $cuenta->beneficiario= strtoupper($request->beneficiario[$i]);
            $cuenta->concepto= strtoupper($request->concepto[$i]);
            $cuenta->justificacion= strtoupper($request->justificacion[$i]);
            $cuenta->monto_programado=str_replace('.','', $request->monto[$i]);
            $cuenta->monto_programado=str_replace(',','.', $cuenta->monto_programado);
            $cuenta->igtf_programado= ($cuenta->monto_programado * 0.075);
            $cuenta->total_programado= $cuenta->monto_programado + $cuenta->igtf_programado; 
            $cuenta->semana=$request->semana;
            $cuenta->nro_orden=$request->nro_orden[$i];
            $cuenta->nro_factura=$request->nro_factura[$i];
            if ($request->fecha_tope[$i] != null) {
                $cuenta->fecha_tope=$request->fecha_tope[$i];    
            }
            if (Auth::user()->rol_id==4) {
                $cuenta->estatus='POR PRE-APROBAR';                
            }
            else{
                $cuenta->estatus='POR APROBAR';
            }
            $cuenta->save();
        }

        Flash::success("LOS REGISTROS SE HAN CREADO CON ÉXITO.");
        return redirect()->route('cuentas_tesoreria.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $Cuenta = Cuenta::findOrFail($id);
            $Cuenta->monto_programado=number_format( $Cuenta->monto_programado, 2, ',', '.');
            return response()->json($Cuenta);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en CuentasTesoreriaController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, 
        [
            'area' =>'required',
            'semana' =>'required',
            'beneficiario' =>'required',
            'concepto' =>'required',
            'monto' =>'required']);

        DB::beginTransaction();
        try {
            $cuenta = Cuenta::findOrFail($id);
            $cuenta->departamento_id=$request->area;
            $cuenta->beneficiario= strtoupper($request->beneficiario);
            $cuenta->concepto= strtoupper($request->concepto);
            $cuenta->justificacion= strtoupper($request->justificacion);
            $cuenta->monto_programado=str_replace('.','', $request->monto);
            $cuenta->monto_programado=str_replace(',','.', $cuenta->monto_programado);
            $cuenta->igtf_programado= ($cuenta->monto_programado * 0.075);
            $cuenta->total_programado= $cuenta->monto_programado + $cuenta->igtf_programado; 
            $cuenta->semana=$request->semana;
            $cuenta->nro_orden=$request->nro_orden;
            $cuenta->nro_factura=$request->nro_factura;
            if ($request->fecha_tope != null) {
                $cuenta->fecha_tope=$request->fecha_tope;    
            }
            $cuenta->updated_at = date('Ymd H:i:s');
            $cuenta->save();
            DB::commit();
            return response()->json($cuenta);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en CuentasTesoreriaController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       DB::beginTransaction();
        try {
            $Cuenta = Cuenta::findOrFail($id);
            $Cuenta->delete();
            DB::commit();
            return response()->json($Cuenta);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en CuentasTesoreriaController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos.'
                ], 500);
        }
    }
    public function listar()
    {
        try {
            $cuentas = Cuenta::where('estatus','POR APROBAR')->orWhere('estatus','PRE-APROBADO')->get();
            $cuentas->each(function($cuentas){
                $cuentas->area=$cuentas->usuario->nombre;
                if ($cuentas->fecha_tope != null) {
                    $cuentas->fecha_tope = date('d-m-Y', strtotime($cuentas->fecha_tope));
                }
               return $cuentas;
            });

            return Datatables::of($cuentas)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en CuentasTesoreriaController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    public function aprobar(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->ids as $id) {
                $cuenta = Cuenta::findOrFail($id); 
                if ($cuenta->estatus == 'POR APROBAR' || $cuenta->estatus =='PRE-APROBADO') {
                    $cuenta->estatus = 'APROBADO';    
                }
                $cuenta->save();
            }
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en CuentasTesoreriaController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }
    public function rechazar(Request $request)
    {
        DB::beginTransaction();
        try {
            foreach ($request->ids as $id) {
                $cuenta = Cuenta::findOrFail($id);
                if ($cuenta->estatus == 'POR APROBAR' || $cuenta->estatus =='PRE-APROBADO') {               
                    $cuenta->estatus = 'RECHAZADO';
                    $cuenta->observacion = 'RECHAZADO POR: PRESIDENCIA';
                }
                $cuenta->save();
            }
            DB::commit();
            return response()->json($request);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en CuentasTesoreriaController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }
    public function por_pagar()
    {
        $cuentas = Cuenta::where('estatus', 'APROBADO')->get();
        $cuentas->each(function($cuentas){
            $cuentas->usuario;
            return $cuentas;
        });
        return view('dashboard.Depto.pago')->with('cuentas',$cuentas);
    }
    public function guardar_pago(Request $request)
    {
        $count=count($request->id);

        for ($i = 0; $i < $count; $i++) {
            $cuenta = Cuenta::findOrFail($request->id[$i]);

            $cuenta->monto_pagado=str_replace('.','', $request->monto[$i]);
            $cuenta->monto_pagado=str_replace(',','.', $cuenta->monto_pagado);
            $cuenta->variacion= $cuenta->monto_programado - $cuenta->monto_pagado;
            $cuenta->igtf_pagado= ($cuenta->monto_pagado * 0.075);
            $cuenta->total_pagado= $cuenta->monto_pagado + $cuenta->igtf_pagado;            
            $cuenta->estatus='PAGADO';
            $cuenta->updated_at = date('Ymd H:i:s');
            $cuenta->save();

            if ($cuenta->variacion > 0 && $cuenta->usuario->rol_id != 4) {
                $nueva_cuenta= new Cuenta;
                $nueva_cuenta->departamento_id=$cuenta->departamento_id;
                $nueva_cuenta->beneficiario= $cuenta->beneficiario;
                $nueva_cuenta->concepto= $cuenta->concepto;
                $nueva_cuenta->monto_programado=$cuenta->variacion;
                $nueva_cuenta->igtf_programado= ($nueva_cuenta->monto_programado * 0.075);
                $nueva_cuenta->total_programado= $nueva_cuenta->monto_programado + $nueva_cuenta->igtf_programado;
                $arrayDias = array( 'Domingo', 'Lunes', 'Martes','Miercoles', 'Jueves', 'Viernes', 'Sabado');                
                if ($arrayDias[date('w')] == 'Viernes') {
                   $domingo = date('d-m-Y', strtotime('+2 day'));
                   $sabado = date('d-m-Y', strtotime('+8 day'));
                }
                if ($arrayDias[date('w')] == 'Sabado') {
                   $domingo = date('d-m-Y', strtotime('+1 day'));
                   $sabado = date('d-m-Y', strtotime('+7 day'));
                }
                if ($arrayDias[date('w')] == 'Domingo') {
                   $domingo = date('d-m-Y', strtotime('+7 day'));
                   $sabado = date('d-m-Y', strtotime('+13 day'));
                }    
                if ($arrayDias[date('w')] == 'Lunes') {
                   $domingo = date('d-m-Y', strtotime('+6 day'));
                   $sabado = date('d-m-Y', strtotime('+12 day'));
                }
                if ($arrayDias[date('w')] == 'Martes') {
                   $domingo = date('d-m-Y', strtotime('+5 day'));               
                   $sabado = date('d-m-Y', strtotime('+11 day'));
                }
                if ($arrayDias[date('w')] == 'Miercoles') {
                   $domingo = date('d-m-Y', strtotime('+4 day'));
                   $sabado = date('d-m-Y', strtotime('+10 day'));
                }
                if ($arrayDias[date('w')] == 'Jueves') {
                   $domingo = date('d-m-Y', strtotime('+3 day'));
                   $sabado = date('d-m-Y', strtotime('+9 day'));
                }
                $proxima_semana= $domingo.' AL '.$sabado;
                $nueva_cuenta->semana= $proxima_semana;
                $nueva_cuenta->estatus='POR APROBAR';
                $nueva_cuenta->save();
            }

        }

        Flash::success("LOS REGISTROS SE HAN PAGADO CON ÉXITO.");
        return redirect()->route('cuentas_tesoreria.index');
    }
}

