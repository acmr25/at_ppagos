<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use App\User;
use App\Cuenta;
use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;
use DB;
use Log;
use Exception;
use File;

use PDF;

class SupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.supervisor.index');
        //
    }
    public function listar()
    {
        try {
            $cuentas = Cuenta::where('estatus','POR PRE-APROBAR')->get(); 
            $cuentas->each(function($cuentas){
                $cuentas->area=$cuentas->usuario->nombre;
                if ($cuentas->fecha_tope != null) {
                    $cuentas->fecha_tope = date('d-m-Y', strtotime($cuentas->fecha_tope));
                }
               return $cuentas;
            });

            return Datatables::of($cuentas)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en CuentasTesoreriaController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function procesar()
    {
        $cuentas = Cuenta::where('estatus', 'POR PRE-APROBAR')->get();
        $cuentas->each(function($cuentas){
            $cuentas->usuario;
            return $cuentas;
        });
        return view('dashboard.supervisor.cambios')->with('cuentas',$cuentas);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function procesado(Request $request)
    {
        $count=count($request->id);
        for ($i = 0; $i < $count; $i++) {
            $id=$request->id[$i];
            $cuenta = Cuenta::find($id);
            if ($request->clase[$id]=='RECHAZADO') {
                $cuenta->estatus='RECHAZADO';    
                $cuenta->observacion=strtoupper($request->observacion[$id]).' RECHAZADO POR:'. Auth::user()->nombre;    
            }
            else {
                $cuenta->estatus='PRE-APROBADO';       
            }
            $cuenta->updated_at = date('Ymd H:i:s');
            $cuenta->save();
        }

        Flash::success("LOS REGISTROS SE HAN PROCESADO CON ÉXITO.");
        return redirect()->route('supervisor.index');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
