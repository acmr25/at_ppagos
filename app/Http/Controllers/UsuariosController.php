<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use App\User;
use App\Rol;


use Yajra\Datatables\Datatables;
use Laracasts\Flash\Flash;

use DB;
use Log;
use Exception;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function listar()
    {
        try {
            $id=Auth::user()->id;
            $usuarios = User::where('id','!=', $id)->get(); 
            $usuarios->each(function($usuarios){
               $usuarios->rol_id = $usuarios->rol->nombre;                
               return $usuarios;
            });

            return Datatables::of($usuarios)->make(true);

        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en UsersController: '.$e->getMessage().', Linea: '.$e->getLine());
            return Datatables::of([])->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
        [
            'nombre'=>'required|min:3|max:45|unique:users,nombre',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'rol_id' => 'required',
            'estado' => 'required']);
        DB::beginTransaction();
        try {
            $usuario = new User;
            $usuario->nombre=$request->nombre;
            $usuario->rol_id=$request->rol_id;
            $usuario->estado=$request->estado;
            $usuario->password = bcrypt($request->password);
            $usuario->save();
            DB::commit();
            return response()->json($usuario);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en UserController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $usuario = User::findOrFail($id);
            return response()->json($usuario);
        } catch (\Exception $e) {
            Log::error('Ha ocurrido un error en UsersController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de obtener los datos.'
                ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, 
            [
            'nombre'=>'required|min:3|max:45|unique:users,nombre,'.$id,
            'password' => 'min:6|confirmed',
            'password_confirmation' => 'min:6',
            'rol_id' => 'required',
            'estado' => 'required']);

         DB::beginTransaction();
        try {
            $usuario = User::findOrFail($id);
            $usuario->nombre=$request->nombre;
            $usuario->rol_id=$request->rol_id;
            $usuario->estado=$request->estado;
            if ($request->password != null) {
                $usuario->password = bcrypt($request->password);
            }
            $usuario->updated_at = date('Ymd H:i:s');
            $usuario->save();
            DB::commit();
            return response()->json($usuario);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en UsersController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de guardar los datos.'
                ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $usuario = User::findOrFail($id);
            $usuario->delete();
            DB::commit();
            return response()->json($usuario);
        } catch (\Exception $e) {
            DB::rollback();
            Log::error('Ha ocurrido un error en UsersController: '.$e->getMessage().', Linea: '.$e->getLine());
            return response()->json([
                'message' => 'Ha ocurrido un error al tratar de eliminar los datos. El usuario debe estar relacionado en alguna operación.'
                ], 500);
        }
    }
}
