<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Yajra\Datatables\Datatables;

use App\Cuenta;

Route::group(['middleware' => 'auth'],function(){
	Route::group(['middleware'=>'activo'],function(){

		Route::get('/', function () {
			if (Auth::user()->rol_id == 5) {
				return redirect()->route('supervisor.index');		
			}
			else{
				return redirect()->route('cuentas_tesoreria.index');
			}
		});

		Route::group(['middleware'=>'Admin_app'],function(){
			Route::get('mantenimiento', function() {
				$li='mantenimiento';
				return view('dashboard.mantenimiento.index')->with('li',$li);		
			});
			Route::get('roles/listar', 'RolesController@listar');
			Route::get('roles/getRoles', 'RolesController@getRoles');
			Route::resource('roles', 'RolesController');
			Route::get('usuarios/listar', 'UsuariosController@listar');
			Route::post('usuarios', ['as' =>'usuarios.store' , 'uses' => 'UsuariosController@store']);
			Route::delete('usuarios/{usuario}', ['as' =>'usuarios.destroy' , 'uses' => 'UsuariosController@destroy']);
		});

		Route::get('usuarios/{usuario}', ['as' =>'usuarios.show' , 'uses' => 'UsuariosController@show']);
		Route::put('usuarios/{usuario}', ['as'=>'usuarios.update' ,'uses' =>'UsuariosController@update']);

		Route::group(['middleware'=>'user'],function(){
			Route::get('cuentas_tesoreria/listar', 'CuentasTesoreriaController@listar');
			Route::put('cuentas_tesoreria/aprobar', 'CuentasTesoreriaController@aprobar');
			Route::put('cuentas_tesoreria/rechazar', 'CuentasTesoreriaController@rechazar');
			Route::get('cuentas_tesoreria/por_pagar', ['as' =>'cuentas_tesoreria.por_pagar' , 'uses' => 'CuentasTesoreriaController@por_pagar']);
			Route::put('cuentas_tesoreria/guardar_pago', ['as' =>'cuentas_tesoreria.guardar_pago' , 'uses' => 'CuentasTesoreriaController@guardar_pago']);
			Route::resource('cuentas_tesoreria', 'CuentasTesoreriaController');
		});

		Route::group(['middleware'=>'supervisor'],function(){
			Route::put('supervisor/procesado', ['as' =>'supervisor.procesado' , 'uses' => 'SupController@procesado']);			
			Route::get('supervisor/procesar',['as' =>'supervisor.procesar' , 'uses' => 'SupController@procesar']);
			Route::get('supervisor/listar', ['as' =>'supervisor.listar' , 'uses' => 'SupController@listar']);
			Route::resource('supervisor', 'SupController');
		});

		Route::get('/cerradas', function () {
			if (Auth::user()->rol_id == 5 || Auth::user()->rol_id == 2) {
				$cuentas=Cuenta::where('estatus','RECHAZADO')->orWhere('estatus','PAGADO')->get();
				$cuentas->each(function($cuentas){
	                $cuentas->area=$cuentas->usuario->nombre;
	                if ($cuentas->fecha_tope != null) {
	                    $cuentas->fecha_tope = date('d-m-Y', strtotime($cuentas->fecha_tope));
	                }
	               return $cuentas;
	            });
				return view('dashboard.cerradas.cerradas')->with('li','cerradas')->with('cuentas',$cuentas);		
			}
			else{
        		Flash::error("NO ESTA AUTORIZADO PARA INGRESAR A ESTA SECCIÓN");
				return redirect()->route('cuentas_tesoreria.index');
			}
		});
		Route::get('/cerradas/listar', function() {
			if (Auth::user()->rol_id == 5 || Auth::user()->rol_id == 2) {
		        try {
			    	$cuentas=Cuenta::where('estatus','RECHAZADO')->orWhere('estatus','PAGADO')->get();
					$cuentas->each(function($cuentas){
		                $cuentas->area=$cuentas->usuario->nombre;
		                if ($cuentas->fecha_tope != null) {
		                    $cuentas->fecha_tope = date('d-m-Y', strtotime($cuentas->fecha_tope));
		                }
		               return $cuentas;
		            });
		            return Datatables::of($cuentas)->make(true);

		        } catch (\Exception $e) {
		            Log::error('Ha ocurrido un error en CuentasTesoreriaController: '.$e->getMessage().', Linea: '.$e->getLine());
		            return Datatables::of([])->make(true);
		        }				
			}
			else{
				Flash::error("NO ESTA AUTORIZADO PARA INGRESAR A ESTA SECCIÓN");
				return redirect()->route('cuentas_tesoreria.index');		
			}

		});

	});

});



Route::auth();
