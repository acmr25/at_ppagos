<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'nombre', 'rol_id','estado','password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];   
    public function cuentas()
    {
        return $this->hasMany('App\Cuenta','departamento_id');
    }
    public function rol()
    {
        return $this->belongsTo('App\Rol','rol_id');
    }
}
