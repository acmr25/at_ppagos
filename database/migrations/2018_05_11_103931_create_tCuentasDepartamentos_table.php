<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTCuentasDepartamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tCuentasDepartamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('departamento_id')->unsigned();
            $table->foreign('departamento_id')->references('id')->on('users')->onUpdate('cascade');
            $table->string('beneficiario');
            $table->string('concepto');
            $table->string('semana');
            $table->date('fecha_tope')->nullable();
            $table->string('estatus')->nullable();
            $table->string('observacion')->nullable();
            $table->string('nro_orden')->nullable();
            $table->string('nro_factura')->nullable();
            $table->decimal('monto_programado', 38, 2);
            $table->decimal('igtf_programado', 38, 2)->nullable();
            $table->decimal('total_programado', 38, 2)->nullable();
            $table->decimal('monto_pagado', 38, 2)->nullable();
            $table->decimal('igtf_pagado', 38, 2)->nullable();
            $table->decimal('total_pagado', 38, 2)->nullable();
            $table->decimal('variacion', 38, 2)->nullable();
            $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('updated_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tCuentasDepartamentos');
    }
}
