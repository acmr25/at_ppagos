<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJustificacionToTcuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tCuentasDepartamentos', function (Blueprint $table) {
            $table->string('justificacion')->nullable();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tCuentasDepartamentos', function (Blueprint $table) {
            $table->dropColumn('justificacion');
        });
    }
}
