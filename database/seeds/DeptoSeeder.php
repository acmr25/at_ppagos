<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;

use App\User;

class DeptoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'nombre'=> 'PRESIDENCIA',
            'rol_id'=> 5,
            'estado'=> 'ACTIVO',
        	'password' => bcrypt(1234),
        ]);
        User::create([
            'nombre'=> 'VICEPRESIDENCIA',
            'rol_id'=> 5,
            'estado'=> 'ACTIVO',
            'password' => bcrypt(1234),
        ]);
        User::create([
        	'nombre'=> 'CAPITAL HUMANO',
            'rol_id'=> 1,
            'estado'=> 'ACTIVO',
        	'password' => bcrypt(1234),
        ]);
        User::create([
        	'nombre'=> 'COMPENSACION Y BENEFICIOS',
            'rol_id'=> 1,
            'estado'=> 'ACTIVO',
        	'password' => bcrypt(1234),
        ]);
        User::create([
            'nombre'=> 'PROCURA',
            'rol_id'=> 4,
            'estado'=> 'ACTIVO',
            'password' => bcrypt(1234),
        ]);
        User::create([
            'nombre'=> 'ADMINISTRACION',
            'rol_id'=> 1,
            'estado'=> 'ACTIVO',
            'password' => bcrypt(1234),
        ]);
        User::create([
            'nombre'=> 'CUENTAS X PAGAR',
            'rol_id'=> 1,
            'estado'=> 'ACTIVO',
            'password' => bcrypt(1234),
        ]);
        User::create([
            'nombre'=> 'TRIBUTOS',
            'rol_id'=> 1,
            'estado'=> 'ACTIVO',
            'password' => bcrypt(1234),
        ]);
        User::create([
            'nombre'=> 'MERCADEO',
            'rol_id'=> 1,
            'estado'=> 'ACTIVO',
            'password' => bcrypt(1234),
        ]);
        User::create([
            'nombre'=> 'JURIDICO',
            'rol_id'=> 1,
            'estado'=> 'ACTIVO',
            'password' => bcrypt(1234),
        ]);
        User::create([
            'nombre'=> 'AUDITORIA',
            'rol_id'=> 1,
            'estado'=> 'ACTIVO',
            'password' => bcrypt(1234),
        ]);
        User::create([
            'nombre'=> 'TESORERIA',
            'rol_id'=> 2,
            'estado'=> 'ACTIVO',
            'password' => bcrypt(1234),
        ]);
        User::create([
            'nombre'=> 'AIT',
            'rol_id'=> 3,
            'estado'=> 'ACTIVO',
            'password' => bcrypt(1234),
        ]);
    }
}
