<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;

use App\Rol;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rol::create([
        	'nombre'=> 'AREA',
        	'descripcion' =>'USUARIO COMUN',
        ]);

        Rol::create([
        	'nombre'=> 'ADMIN_APP',
        	'descripcion' =>'USUARIO CON PERMISOS ESPECIALES ',
        ]);
        Rol::create([
        	'nombre'=> 'ADMIN_USER',
        	'descripcion' =>'USUARIO ADMINISTRADOR DE CUENTAS Y ROLES ',
        ]);
        Rol::create([
            'nombre'=> 'AREA_ESPECIAL',
            'descripcion' =>'SIN LIMITE DE TIEMPO PARA EDITAR Y ELIMINAR SIEMPRE Y CUANDO ESTE SIN APROBAR',
        ]);
        Rol::create([
            'nombre'=> 'SUPERVISOR',
            'descripcion' =>'USUARIO PRE-APROBADOR DE LA PROGRAMACIÓN DE PAGOS , VISUALIZADOR DE TODOS LOS REGISTROS REALIZADOS POR EL RESTO DE LOS USUARIOS',
        ]);
    }
}
