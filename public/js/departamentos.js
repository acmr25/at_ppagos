
  var ruta_cuenta = ruta+'/cuentas_tesoreria';

  $.fn.dataTable.ext.errMode = 'throw';


  var tabla =$('#example1').DataTable({
      dom: 
        "<'row'<'col-sm-1'B><'col-sm-2 text-center'l><'col-sm-9'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      language: leng,
      "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
      iDisplayLength: -1,
      order: [[ 0, "desc" ]],
      "bAutoWidth": false,
      colReorder: true,
      "scrollX": true,
      "columnDefs": [{ "orderable": false, "targets": [8] }],
      initComplete: function () {
        this.api().columns([0,1,2,4,8]).every( function () {
            var column = this;
            var select = $('<select><option value=""></option></select>')
                .appendTo( $(column.footer()).empty() )
                .on( 'change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );
                    column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                } );

            column.data().unique().sort().each( function ( d, j ) {
                select.append( '<option value="'+d+'">'+d+'</option>' )
            } );
        } );
      },
      buttons: [
            {
              extend: 'excelHtml5',
              exportOptions: {
                  columns: [0,1,2,3,4,5,6,7,8,9,10,11,12],
                  search: 'applied',
                  order: 'applied' //column id visible in PDF 
                } 
            },
        ],

  })

  function showCuenta(id){
  $.ajax({
    url: ruta_cuenta+'/'+id,
    type: 'GET',
    success: function(res){ 
      $('#cuenta_id').val(res.id);
      $('#area').val(res.departamento_id);
      $('#semana').val(res.semana);
      $('#beneficiario').val(res.beneficiario);     
      $('#concepto').val(res.concepto);
      $('#justificacion').val(res.justificacion);
      $('#monto').val(res.monto_programado);
      $('#fecha_tope').val(res.fecha_tope); 
      $('#nro_orden').val(res.nro_orden); 
      $('#nro_factura').val(res.nro_factura);  
      $('#modal-cuenta').modal('show'); 
    },
    error: function(jqXHR, textStatus, errorThrown) {
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de obtener los datos. Status: '+jqXHR.status,
        'error'
        )
    }
  });
}

function deleteCuenta(id){

  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = ruta_cuenta+"/"+id;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          success: function(res){ 
            sweetAlert({
              title: 'Exito!',
              text: 'Se han eliminado los datos de forma exitosa! ',
              type: 'success',
              showConfirmButton: false,
              timer: 2000
            })
            setTimeout("location.href= ruta_cuenta", 2050);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar el registro. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'El registro se ha eliminado exitosamente',
      'success'
      );
  });
}

function Cuenta(){
  this.id = $('#cuenta_id').val();
  this.area = $('#area').val();
  this.semana = $('#semana').val();
  this.beneficiario = $('#beneficiario').val();
  this.concepto = $('#concepto').val();
  this.justificacion = $('#justificacion').val();
  this.monto = $('#monto').val();
  this.fecha_tope = $('#fecha_tope').val();
  this.nro_orden = $('#nro_orden').val();
  this.nro_factura = $('#nro_factura').val();
}

function starLoad(btn){
  $(btn).button('loading');
  $('.load-ajax').addClass('overlay');
  $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
  $(btn).button('reset');
  $('.load-ajax').removeClass('overlay');
  $('.load-ajax').fadeIn(1000).html("");
} 

// --------------------------------------------------PARA ERRORES----------------------------------------------------------------------

function removeStyleCuenta(){
  $('#field-area').removeClass("has-error");
  $('#field-area .msj-error').html("");

  $('#field-semana').removeClass("has-error");
  $('#field-semana .msj-error').html("");

  $('#field-beneficiario').removeClass("has-error");
  $('#field-beneficiario .msj-error').html("");

  $('#field-concepto').removeClass("has-error");
  $('#field-concepto .msj-error').html("");

  $('#field-justificacion').removeClass("has-error");
  $('#field-justificacion .msj-error').html("");

  $('#field-monto').removeClass("has-error");
  $('#field-monto .msj-error').html("");
}

$('#guardar-cuenta').click(function(){
  var type = "";
  var route = "";
  var btn = this
  starLoad(btn)
  var data = new Cuenta();
  type = 'PUT';
  route = ruta_cuenta+'/'+data.id
  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: type,
    dataType: 'json',
    data: data,
    success: function(res){ 
      endLoad(btn);
      sweetAlert({
        title: 'Exito!',
        text: 'Se han actualizado los datos de forma exitosa! ',
        type: 'success',
        showConfirmButton: false,
        timer: 2000
      })
      setTimeout("location.href= ruta_cuenta", 2050);
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad(btn)
      if(jqXHR.status == 422){
        removeStyleCuenta()
          if(jqXHR.responseJSON.area){         
            $('#field-area').addClass("has-error");
            $('#field-area .msj-error').html(jqXHR.responseJSON.area);
          }          
          if(jqXHR.responseJSON.semana){         
            $('#field-semana').addClass("has-error");
            $('#field-semana .msj-error').html(jqXHR.responseJSON.semana);
          }
          if(jqXHR.responseJSON.beneficiario){         
            $('#field-beneficiario').addClass("has-error");
            $('#field-beneficiario .msj-error').html(jqXHR.responseJSON.beneficiario);
          }
          if(jqXHR.responseJSON.concepto){         
            $('#field-concepto').addClass("has-error");
            $('#field-concepto .msj-error').html(jqXHR.responseJSON.concepto);
          }
          if(jqXHR.responseJSON.justificacion){         
            $('#field-justificacion').addClass("has-error");
            $('#field-justificacion .msj-error').html(jqXHR.responseJSON.justificacion);
          }
          if(jqXHR.responseJSON.monto){         
            $('#field-monto').addClass("has-error");
            $('#field-monto .msj-error').html(jqXHR.responseJSON.monto);
          }
      }
    else{
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
        'error'
        )
      }

    }
  });
});

$(document).ready(function() {
  $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
}); 
