
  var ruta_cuenta = ruta+'/supervisor';

  $.fn.dataTable.ext.errMode = 'throw';

  var tabla =$('#example1').DataTable({
      dom: 
        "<'row'<'col-sm-1'B><'col-sm-2 text-center'l><'col-sm-9'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      processing: true,
      serverSide: true,
      ajax: ruta_cuenta+'/listar',
      search: { "caseInsensitive": true },
      columns: [
      { data: 'id', name: 'id'},
      { data: 'semana', name: 'semana'},
      { data: 'area', name: 'area'},
      { data: 'beneficiario', name: 'beneficiario'},
      { data: 'concepto', name: 'concepto'},
      { data: 'justificacion', name: 'justificacion'},
      { data: 'fecha_tope', name: 'fecha_tope'},
      { data: 'nro_orden', name: 'nro_orden'},
      { data: 'nro_factura', name: 'nro_factura'},
      { data: 'monto_programado', name: 'monto_programado'},
      ],      

      language: leng,
      order: [[ 0, "desc" ]],
      "bAutoWidth": false,
      "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
      iDisplayLength: -1,
      "scrollX": true,
      colReorder: true,
      buttons: [
            {
              extend: 'excelHtml5',
              exportOptions: {
                  columns: [0,1,2,3,4,5,6,7,8,9],
                  search: 'applied',
                  order: 'applied' //column id visible in PDF 
                } 
            },
        ],
  })

  $('#actualizar-tabla').click(function(){
    tabla.ajax.reload();
  })

$(document).ready(function() {
  $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
}); 