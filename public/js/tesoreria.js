
  var ruta_cuenta = ruta+'/cuentas_tesoreria';

  $.fn.dataTable.ext.errMode = 'throw';

  var tabla =$('#example1').DataTable({
      dom: 
        "<'row'<'col-sm-1'B><'col-sm-2 text-center'l><'col-sm-9'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      processing: true,
      serverSide: true,
      ajax: ruta_cuenta+'/listar',
      search: { "caseInsensitive": true },
      columns: [
      {
        'targets': 0,
        'searchable': false,
        'orderable': false,
        'className': 'text-center',
        'render': function (data, type, full, meta){
          return '<input type="checkbox" name="checkbox-lista" value="' + full.id + '">';
        }
      },
      { data: 'id', name: 'id'},
      { data: 'semana', name: 'semana'},
      { data: 'area', name: 'area'},
      { data: 'beneficiario', name: 'beneficiario'},
      { data: 'concepto', name: 'concepto'},
      { data: 'fecha_tope', name: 'fecha_tope'},
      { data: 'nro_orden', name: 'nro_orden'},
      { data: 'nro_factura', name: 'nro_factura'},
      { data: 'estatus', name: 'estatus'},
      { data: 'monto_programado', name: 'monto_programado'},
      { data: 'igtf_programado', name: 'igtf_programado'},
      { data: 'total_programado', name: 'total_programado'},
      { data: 'monto_pagado', name: 'monto_pagado'},
      { data: 'igtf_pagado', name: 'igtf_pagado'},
      { data: 'total_pagado', name: 'total_pagado'},
      { data: 'variacion', name: 'variacion'},
      { data: 'id',
        'orderable': false,
        render: function ( data, type, full, meta ) {
          var text = '';
          if (full.estatus == 'POR APROBAR') {
            text += '<div class="btn-group" style="width: 50px">'+
            '<button type="button" class="btn btn-warning btn-xs" title="Editar" OnClick="showCuenta('+data+')">'+
              '<i class="fa fa-edit"></i>'+
            '</button>'+
            '<button type="button" class="btn btn-danger btn-xs"title="Eliminar" style="margin-left:2.5px;" OnClick="deleteCuenta('+data+')">'+
              '<i class="fa fa-remove"></i>'+
            '</button>'+
            '</div>'; 
          }
          return text;
        }
      }
      ],      

      language: leng,
      order: [[ 1, "desc" ]],
      "bAutoWidth": false,
      "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
      iDisplayLength: -1,
      "scrollX": true,
      colReorder: true,
      "columnDefs": [{ "orderable": false, "targets": [0] }],
      initComplete: function () {
        this.api().columns([1,2,3,4,5,9]).every( function () {
            var column = this;
            var select = $('<select><option value=""></option></select>')
                .appendTo( $(column.footer()).empty() )
                .on( 'change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );

                    column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                } );

            column.data().unique().sort().each( function ( d, j ) {
                select.append( '<option value="'+d+'">'+d+'</option>' )
            } );
        } );
      },
      "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
      buttons: [
            {
              extend: 'excelHtml5',
              exportOptions: {
                  columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],
                  search: 'applied',
                  order: 'applied' //column id visible in PDF 
                } 
            },
        ],
  })

  $('#actualizar-tabla').click(function(){
    tabla.ajax.reload();
  })

  function showCuenta(id){
  $.ajax({
    url: ruta_cuenta+'/'+id,
    type: 'GET',
    success: function(res){ 
      $('#cuenta_id').val(res.id);
      $('#area').val(res.departamento_id);
      $('#semana').val(res.semana);
      $('#beneficiario').val(res.beneficiario);     
      $('#concepto').val(res.concepto);
      $('#monto').val(res.monto_programado); 
      $('#fecha_tope').val(res.fecha_tope); 
      $('#nro_orden').val(res.nro_orden); 
      $('#nro_factura').val(res.nro_factura); 
      $('#modal-cuenta').modal('show'); 
    },
    error: function(jqXHR, textStatus, errorThrown) {
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de obtener los datos. Status: '+jqXHR.status,
        'error'
        )
    }
  });
}

function deleteCuenta(id){

  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = ruta_cuenta+"/"+id;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          success: function(res){ 
            resolve()
            $('#modal-cuenta').modal('hide');
            tabla.ajax.reload();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar el registro. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'El registro se ha eliminado exitosamente',
      'success'
      );
  });
}

function Cuenta(){
  this.id = $('#cuenta_id').val();
  this.area = $('#area').val();
  this.semana = $('#semana').val();
  this.beneficiario = $('#beneficiario').val();
  this.concepto = $('#concepto').val();
  this.monto = $('#monto').val();
  this.fecha_tope = $('#fecha_tope').val();
  this.nro_orden = $('#nro_orden').val();
  this.nro_factura = $('#nro_factura').val();
}

function starLoad(btn){
  $(btn).button('loading');
  $('.load-ajax').addClass('overlay');
  $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
  $(btn).button('reset');
  $('.load-ajax').removeClass('overlay');
  $('.load-ajax').fadeIn(1000).html("");
} 

// --------------------------------------------------PARA ERRORES----------------------------------------------------------------------

function removeStyleCuenta(){
  $('#field-area').removeClass("has-error");
  $('#field-area .msj-error').html("");

  $('#field-semana').removeClass("has-error");
  $('#field-semana .msj-error').html("");

  $('#field-beneficiario').removeClass("has-error");
  $('#field-beneficiario .msj-error').html("");

  $('#field-concepto').removeClass("has-error");
  $('#field-concepto .msj-error').html("");

  $('#field-monto').removeClass("has-error");
  $('#field-monto .msj-error').html("");
}

$('#guardar-cuenta').click(function(){
  var type = "";
  var route = "";
  var btn = this
  starLoad(btn)
  var data = new Cuenta();
  type = 'PUT';
  route = ruta_cuenta+'/'+data.id
  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: type,
    dataType: 'json',
    data: data,
    success: function(res){ 
      endLoad(btn);
      $('#modal-cuenta').modal('hide');
      swal(
      'Actualizado!!',
      'El registro se ha actualizado exitosamente',
      'success'
      );
      tabla.ajax.reload();
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad(btn)
      if(jqXHR.status == 422){
        removeStyleCuenta()
          if(jqXHR.responseJSON.area){         
            $('#field-area').addClass("has-error");
            $('#field-area .msj-error').html(jqXHR.responseJSON.area);
          }          
          if(jqXHR.responseJSON.semana){         
            $('#field-semana').addClass("has-error");
            $('#field-semana .msj-error').html(jqXHR.responseJSON.semana);
          }
          if(jqXHR.responseJSON.beneficiario){         
            $('#field-beneficiario').addClass("has-error");
            $('#field-beneficiario .msj-error').html(jqXHR.responseJSON.beneficiario);
          }
          if(jqXHR.responseJSON.concepto){         
            $('#field-concepto').addClass("has-error");
            $('#field-concepto .msj-error').html(jqXHR.responseJSON.concepto);
          }
          if(jqXHR.responseJSON.monto){         
            $('#field-monto').addClass("has-error");
            $('#field-monto .msj-error').html(jqXHR.responseJSON.monto);
          }
      }
    else{
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
        'error'
        )
      }

    }
  });
});


$('#modal-cuenta').on('hidden.bs.modal', function (e) 
  {
    tabla.ajax.reload();
    $('#form-cuenta')[0].reset();
    $('#cuenta_id').val('');
    removeStyleCuenta();
  });

// Handle click on "Select all" control
$('#select-all-cuentas').on('click', function(){
  // Get all rows with search applied
  var rows = tabla.rows({ 'search': 'applied' }).nodes();
  // Check/uncheck checkboxes for all rows in the table
  $('input[type="checkbox"]', rows).prop('checked', this.checked);
}); 

$('#btn-aprobar-select').click(function(){
  var data = {
    ids: tabla.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
  };
  if(data.ids.length > 0){
    swal({
      text: "¿Estás seguro que desea aprobar estos registros?",
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      showLoaderOnConfirm: true,
      preConfirm: function() {
        return new Promise(function(resolve, reject) {
          
          $.ajax({
            url: ruta_cuenta+'/aprobar',
            type: 'PUT',
            data: data,
            headers: {'X-CSRF-TOKEN': $('#token').val()},
            success: function(res){ 
              resolve()
              tabla.ajax.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
              swal(
                'Error',
                'Ha ocurrido un error al tratar de modificar los datos. Status: '+jqXHR.status,
                'error'
                )
            }
          })
        });
      },
      allowOutsideClick: false
    }).then(function() {
      swal(
        'Actualizado!',
        'Los registros seleccionados se han actualizado exitosamente',
        'success'
        );
    });
  }else{
    swal(
      'Atención!',
      'Debe de seleccionar un registro para porder realizar esta acción',
      'warning'
      );
  }
});

$('#btn-rechazar-select').click(function(){
  var data = {
    ids: tabla.$('input[name="checkbox-lista"]:checked').map(function() { return this.value; }).get()
  };
  if(data.ids.length > 0){
    swal({
      text: "¿Estás seguro que desea rechazar estos registros?",
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      showLoaderOnConfirm: true,
      preConfirm: function() {
        return new Promise(function(resolve, reject) {          
          $.ajax({
            url: ruta_cuenta+'/rechazar',
            type: 'PUT',
            data: data,
            headers: {'X-CSRF-TOKEN': $('#token').val()},
            success: function(res){ 
              resolve()
              tabla.ajax.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
              swal(
                'Error',
                'Ha ocurrido un error al tratar de modificar los datos. Status: '+jqXHR.status,
                'error'
                )
            }
          })
        });
      },
      allowOutsideClick: false
    }).then(function() {
      swal(
        'Actualizado!',
        'Los registros seleccionados se han rechazado exitosamente',
        'success'
        );
    });
  }else{
    swal(
      'Atención!',
      'Debe de seleccionar un registro para porder realizar esta acción',
      'warning'
      );
  }
});


$(document).ready(function() {
  $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
}); 