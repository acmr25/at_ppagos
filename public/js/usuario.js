function Usuario(){
  this.id = $('#usuario_id').val();
  this.nombre = $('#nombre').val();
  this.password = $('#password').val();
  this.password_confirmation = $('#password_confirmation').val();
  this.rol_id = $('#rol_id').val();
  this.estado = $('#estado').val();
}

function starLoad_cargarsito(btn){
  $(btn).button('loading');
  $('.load-ajax').addClass('overlay');
  $('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad_descargansito(btn){
  $(btn).button('reset');
  $('.load-ajax').removeClass('overlay');
  $('.load-ajax').fadeIn(1000).html("");
} 

function removeStyleUsuario(){
  $('#field-nombre-user').removeClass("has-error");
  $('#field-nombre-user .msj-error').html("");

  $('#field-password').removeClass("has-error");
  $('#field-password .msj-error').html("");

  $('#field-password_confirmation').removeClass("has-error");
  $('#field-password_confirmation .msj-error').html("");

  $('#CAMPO_ROL').removeClass("has-error");
  $('#CAMPO_ROL .msj-error').html("");

  $('#CAMPO_ESTADO').removeClass("has-error");
  $('#CAMPO_ESTADO .msj-error').html("");

}

function UsuarioActual(id){
  $.ajax({
    url: ruta+'/usuarios/'+id,
    type: 'GET',
    success: function(res){ 
      $('#usuario_id').val(res.id);
      $('#nombre').val(res.nombre); 
      $('#rol_id').val(res.rol_id);
      $('#estado').val(res.estado);
      $('#modal-usuario').modal('show'); 
    },
    error: function(jqXHR, textStatus, errorThrown) {
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de obtener los datos. Status: '+jqXHR.status,
        'error'
        )
    }
  });
}

$('#guardar-usuario').click(function(){
  var type = "";
  var route = "";
  var btn = this
  starLoad_cargarsito(btn)
  var data = new Usuario();
  if(data.id == "" || data.id == null || data.id == undefined){
    type = 'POST';
    route = ruta+'/usuarios';
  }else{
    type = 'PUT';
    route = ruta+'/usuarios/'+data.id
  }
  $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': $('#token').val()},
    type: type,
    dataType: 'json',
    data: data,
    success: function(res){
      endLoad_descargansito(btn)
      removeStyleUsuario();
      $('#modal-usuario').modal('hide');
      sweetAlert(
        'Exito!',
        'Se han guardados los datos de forma exitosa! ',
        'success'
        )
    },
    error: function(jqXHR, textStatus, errorThrown){
      endLoad_descargansito(btn)
      if(jqXHR.status == 422){
        removeStyleUsuario()        
          if(jqXHR.responseJSON.nombre){         
            $('#field-nombre-user').addClass("has-error");
            $('#field-nombre-user .msj-error').html(jqXHR.responseJSON.nombre);
          }
          if(jqXHR.responseJSON.password){         
             $('#field-password').addClass("has-error");
            $('#field-password .msj-error').html(jqXHR.responseJSON.password);
          }
          if(jqXHR.responseJSON.password_confirmation){         
            $('#field-password_confirmation').addClass("has-error");
            $('#field-password_confirmation .msj-error').html(jqXHR.responseJSON.password_confirmation);
          }
          if(jqXHR.responseJSON.rol_id){         
            $('#CAMPO_ROL').addClass("has-error");
            $('#CAMPO_ROL .msj-error').html(jqXHR.responseJSON.rol_id);
          }
          if(jqXHR.responseJSON.estado){         
            $('#CAMPO_ESTADO').addClass("has-error");
            $('#CAMPO_ESTADO .msj-error').html(jqXHR.responseJSON.estado);
          }
      }
    else{
      sweetAlert(
        'Error',
        'Ha ocurrido un error al tratar de guardar los datos. Status: '+jqXHR.status,
        'error'
        )
      }

    }
  });
});

$('#modal-usuario').on('hidden.bs.modal', function (e) 
{ 
  $('#form-usuario')[0].reset();
  $('#usuario_id').val('');
  removeStyleUsuario();
  tabla_usuarios.ajax.reload();
});