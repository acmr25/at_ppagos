## App de Programación de Pagos

Aplicación de programación de pagos para que los distintos departamentos de una empresa anexen en una misma vía las distintas solicitudes de pagos para sus actividades. A traves de ella la gerencia aprueba o rechaza y establece el monto para las distintas solicitudes y los departamentos puedan confirmar el status de cada soliditud.


