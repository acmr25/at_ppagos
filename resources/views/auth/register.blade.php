<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>@yield('title', 'Amazonas Tech')</title>

    <link rel="stylesheet" href="{{ asset('public/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/plugins/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/plugins/Ionicons/css/ionicons.min.css') }}">

    @yield('css')

    <link rel="stylesheet" href="{{ asset('public/plugins/adminlte/css/AdminLTE.min.css') }}">

    <link rel="stylesheet" href="{{ asset('public/plugins/adminlte/css/skins/_all-skins.min.css') }}">


</head>

<body class="hold-transition login-page" style="background-image: url('{{ asset('public/image/fondo.jpg') }}');background-size: cover;">

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" style="padding-top: 150px">
    <div class="login-box">
      <div class="login-box-body">
        <center>Registro de Departamentos</center><hr />
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}


                <div class="{{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-user"></i></span>
                      {!! Form::text('nombre', null, ['class'=>'form-control', 'id'=>'nombre', 'name'=>'nombre','required' => 'required','placeholder'=>'Nombre del departamento']) !!}
                    </div>
                    @if ($errors->has('nombre'))
                      <span class="help-block">
                          <strong>{{ $errors->first('nombre') }}</strong>
                      </span>
                @endif
                </div>
                <br/>
                <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
                      {!! Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Contraseña']) !!}
                    </div>
                    @if ($errors->has('password'))
                      <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                      </span>
                    @endif
                </div>
                <br/>
                <div class="{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
                      {!! Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'Confrmación de Contraseña']) !!}
                    </div>
                    @if ($errors->has('password_confirmation'))
                      <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                      </span>
                    @endif
                </div>
                <br/>
                <button type="submit" class="btn btn-primary btn-block btn-flat">
                    <i class="fa fa-btn fa-user"></i> Registrar
                </button>
            </form>
        <br />
        <a class="btn" href="{{ url('/login') }}">Ya estoy registrad@</a>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->  
        </div>
    </div>
</div>
<!-- jQuery 3 -->
<script src="{{ asset('public/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('public/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('public/plugins/adminlte/js/adminlte.min.js') }}"></script>


</body>
</html>