<!-- Modal -->
<div class="modal fade" id="modal-cuenta" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open(['id'=>'form-cuenta']) !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">  </h4>
      </div>
      <div class="modal-body">
        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('id', null, ['id'=>'cuenta_id']) !!} 
        @if(Auth::user()->rol_id == 2) 
          <div class="form-group" id="field-area">
            {!! Form::label('area', 'Area:') !!}
            {!! Form::select('area', $areas, null, ['id' => 'area', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
            <span><strong class="text-danger msj-error"></strong></span>
          </div>
        @else
            {!! Form::hidden('area', null, ['id'=>'area']) !!} 
        @endif        
        <div class="form-group" id="field-semana">
          {!! Form::label('semana', 'Semana del:') !!}
          {!! Form::text('semana', null, ['id' => 'semana','class' => 'form-control col-sm-4', 'required' => 'required', 'readonly']) !!}
          <small class="text-danger">{{ $errors->first('semana') }}</small>
        </div>
        <div class="form-group{{ $errors->has('beneficiario') ? ' has-error' : '' }}" id="field-beneficiario">
            {!! Form::label('beneficiario', 'Beneficiario:') !!}
            {!! Form::text('beneficiario', null, ['id'=>'beneficiario','class' => 'form-control', 'required' => 'required']) !!}
            <small class="text-danger">{{ $errors->first('beneficiario') }}</small>
        </div>
        <div class="form-group{{ $errors->has('concepto') ? ' has-error' : '' }}"  id="field-concepto">
            {!! Form::label('concepto', 'Concepto:') !!}
            {!! Form::text('concepto', null, ['id'=>'concepto','class' => 'form-control', 'required' => 'required']) !!}
            <small class="text-danger">{{ $errors->first('concepto') }}</small>
        </div>
        <div class="form-group{{ $errors->has('justificacion') ? ' has-error' : '' }}"  id="field-justificacion">
            {!! Form::label('justificacion', 'justificación:') !!}
            {!! Form::text('justificacion', null, ['id'=>'justificacion','class' => 'form-control', 'required' => 'required']) !!}
            <small class="text-danger">{{ $errors->first('justificacion') }}</small>
        </div>
        <div class="form-group{{ $errors->has('monto') ? ' has-error' : '' }}" id="field-monto">
            {!! Form::label('monto', 'Monto:') !!}
            <input type="text" class="form-control numero" placeholder="Monto de la Operación" required="true" name="monto[]" id="monto" name="monto" value="null" dir="rtl">
            <small class="text-danger">{{ $errors->first('monto') }}</small>
        </div>
        <div class="form-group{{ $errors->has('fecha_tope') ? ' has-error' : '' }}">
            {!! Form::label('fecha_tope', 'Fecha tope de pago:') !!}
            {!! Form::date('fecha_tope', null, ['id'=>'fecha_tope','class' => 'form-control', 'required' => 'required']) !!}
            <small class="text-danger">{{ $errors->first('fecha_tope') }}</small>
        </div>
        <div class="form-group{{ $errors->has('nro_orden') ? ' has-error' : '' }}">
            {!! Form::label('nro_orden', 'Nro. de Orde de Compra (Profit):') !!}
            {!! Form::text('nro_orden', null, ['id'=>'nro_orden','class' => 'form-control', 'placeholder'=>'---','onkeypress'=>'return soloNumeros(event)']) !!}
            <small class="text-danger">{{ $errors->first('nro_orden') }}</small>
        </div>
        <div class="form-group{{ $errors->has('nro_factura') ? ' has-error' : '' }}">
            {!! Form::label('nro_factura', 'Nro. de Factura (Profit):') !!}
            {!! Form::text('nro_factura', null, ['id'=>'nro_factura','class' => 'form-control', 'placeholder'=>'---','onkeypress'=>'return soloNumeros(event)']) !!}
            <small class="text-danger">{{ $errors->first('nro_factura') }}</small>
        </div>
      </div>

      <div class="modal-footer">
        <div >
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-cuenta" class="btn btn-success pull-right" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>      
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

