@extends('templates.master')

@section('css')
  <link rel="stylesheet" href="{{ asset('public/plugins/select2/dist/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
  <style type="text/css">
    .bootstrap-datetimepicker-widget tr:hover {
    background-color: #808080;
}
  </style>
@endsection

@section('titulo_modulo','Formulario')

@section('contenido')
<div class="row" >

  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
          <div class="col-md-12">
            {!! Form::open(['method' => 'POST', 'route' => 'cuentas_tesoreria.store', 'class' => 'form-horizontal']) !!}
              @if(Auth::user()->rol_id == 2) 
                <div class="form-group{{ $errors->has('area') ? ' has-error' : '' }}">
                    {!! Form::label('area', 'Area:') !!}
                    {!! Form::select('area', $areas, null, ['id' => 'area', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
                    <small class="text-danger">{{ $errors->first('area') }}</small>
                </div>
              @else
                {!! Form::hidden('area', Auth::user()->id , ['id' => 'area_id']) !!}
              @endif
              <div class="form-group{{ $errors->has('semana') ? ' has-error' : '' }}">
                  {!! Form::label('semana', 'Semana del:') !!}
                  {!! Form::text('semana', null, ['id'=>'semana', 'class' => 'form-control col-sm-4', 'required' => 'required', 'placeholder'=>'Seleccione la semana ']) !!}
                  <small class="text-danger">{{ $errors->first('semana') }}</small>
                  <small class="help-block">LA SELECCIÓN DE LA SEMANA DEBE APARECER CON EL SIGUIENTE FORMATO: DD-MM-AAAA AL DD-MM-AAAA</small>
              </div>
              <table id="myTable" class=" table order-list text-center">
                <thead>
                    <tr>
                        <th>Beneficiario</th>
                        <th>Concepto</th>
                        <th>Justificación</th>
                        <th>Monto</th>
                        <th>Fecha Tope de pago</th>
                        <th>Nro. Orden de Compra</th>
                        <th>Nro. Factura (Profit)</th>
                    </tr>
                </thead>
                <tbody>
                  @if(Auth::user()->rol_id == 2) 
                    <tr>
                      <td >
                        {!! Form::text('beneficiario[]', null, ['class' => 'form-control', 'placeholder'=>'Nombre del Beneficiario', 'required' => 'required']) !!}
                      </td>
                      <td>
                        {!! Form::text('concepto[]', null, ['class' => 'form-control', 'placeholder'=>'Concepto de la operación', 'required' => 'required']) !!}
                      </td>
                      <td>
                        {!! Form::text('justificacion[]', null, ['class' => 'form-control', 'placeholder'=>'Justificación de la operación']) !!}
                      </td>
                      <td width="15%">
                        <input type="text" class="form-control numero" placeholder="----" required="true" name="monto[]" dir="rtl" onkeypress="return soloNumeros(event)">
                      </td>
                      <td  width="10%">
                        {!! Form::date('fecha_tope[]', null, ['class' => 'form-control', 'placeholder'=>'Fecha tope de pago']) !!}
                      </td>
                      <td width="8%">
                        {!! Form::text('nro_orden[]', null, ['class' => 'form-control', 'placeholder'=>'---','onkeypress'=>'return soloNumeros(event)']) !!}
                      </td>
                      <td width="8%">
                        {!! Form::text('nro_factura[]', null, ['class' => 'form-control', 'placeholder'=>'---','onkeypress'=>'return soloNumeros(event)']) !!}
                      </td>
                      <td><a class="deleteRow"></a>
                      </td>
                    </tr>
                  @else
                    <tr>
                        <td >
                          {!! Form::select('beneficiario[]', $obeneficiarios, null, ['id' => 'beneficiario','class' => 'form-control select2','required' => 'required']) !!}
                        </td>
                        <td>
                          {!! Form::text('concepto[]', null, ['class' => 'form-control', 'placeholder'=>'Concepto de la operación', 'required' => 'required']) !!}
                        </td>
                        <td>
                          {!! Form::text('justificacion[]', null, ['class' => 'form-control', 'placeholder'=>'Justificación de la operación']) !!}
                        </td>
                        <td width="15%">
                          <input type="text" class="form-control numero" placeholder="----" required="true" name="monto[]" dir="rtl" onkeypress="return soloNumeros(event)">
                        </td>
                        <td  width="10%">
                          {!! Form::date('fecha_tope[]', null, ['class' => 'form-control', 'placeholder'=>'Fecha tope de pago']) !!}
                        </td>
                        <td width="8%">
                          {!! Form::text('nro_orden[]', null, ['class' => 'form-control', 'placeholder'=>'---','onkeypress'=>'return soloNumeros(event)']) !!}
                        </td>
                        <td width="8%">
                          {!! Form::text('nro_factura[]', null, ['class' => 'form-control', 'placeholder'=>'---', 'onkeypress'=>'return soloNumeros(event)']) !!}
                        </td> 
                        <td><a class="deleteRow"></a>

                        </td>
                    </tr>
                  @endif
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6" >
                          @if(Auth::user()->rol_id == 2)
                            <input type="button" class="btn btn-block btn-sm btn-info " id="addrow" value="Agregar fila" />
                          @else
                            <input type="button" class="btn btn-block btn-sm btn-info " id="addrow_areas" value="Agregar fila" />
                          @endif
                        </td>
                    </tr>
                </tfoot>
              </table>
              <div class="col-md-12">
                <br />
                  <a href="{{ URL::previous() }}" class="btn btn-danger">Cancelar</a>
                  <input type="submit" value="Guardar" class='btn btn-success pull-right' onclick="return confirm('¿Está seguro que quiere guardar estos registros? Nota: Solo se podrá modificar o eliminar mientras la semana en curso sea igual a la de los registros y antes de la fecha de corte asignada al area.');" >
              </div>
            {!! Form::close() !!}
          </div>
      </div>
</div>
@endsection

@section('js')
<script src="{{ asset('public/plugins/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('public/plugins/select2/dist/js/i18n/es.js') }}"></script>

<!-- date-range-picker -->
<script src="{{ asset('public/plugins/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/plugins/moment/min/moment-with-locales.min.js') }}"></script>

<script src="{{ asset('public/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

<script type="text/javascript">  

$('.select2').select2({
  placeholder: "Seleccione",
  tags:true,
  createTag: function (params) {
    return {
      id: params.term,
      text: params.term,
      newOption: true }
    },
  templateResult: function (data) {
    var $result = $("<span></span>");
    $result.text(data.text);
    if (data.newOption) {
      $result.append(" <em>(Nuevo)</em>");
    }
    return $result;
  },
  width:'100%',
  language: "es",
}).val(null).trigger('change');

var counter = 1;

$("#addrow_areas").on("click", function () {
    var newRow = $('<tr>', { id: 'row'+counter});
    var cols = "";
    cols += '<td>{!! Form::select('beneficiario[]',  $obeneficiarios, null, ['class' => 'form-control new select2', 'required' => 'required']) !!}</td>';
    cols += '<td>{!! Form::text('concepto[]', null, ['class' => 'form-control', 'placeholder'=>'Concepto de la operación', 'required' => 'required']) !!}</td>';
    cols += '<td>{!! Form::text('justificacion[]', null, ['class' => 'form-control', 'placeholder'=>'Justificación de la operación', 'required' => 'required']) !!}</td>';
    cols += '<td width="15%"><input type="text" class="form-control numero" placeholder="----" required="true" id="'+counter+'" name="monto[]" dir="rtl" onkeypress="return soloNumeros(event)"></td>';
    cols += '<td width="10%">{!! Form::date('fecha_tope[]', null, ['class' => 'form-control', 'placeholder'=>'Fecha tope de pago']) !!}</td>';
    cols += '<td width="8%">{!! Form::text('nro_orden[]', null, ['class' => 'form-control', 'placeholder'=>'---','onkeypress'=>'return soloNumeros(event)']) !!}</td>';
    cols += '<td width="8%"> {!! Form::text('nro_factura[]', null, ['class' => 'form-control', 'placeholder'=>'---', 'onkeypress'=>'return soloNumeros(event)']) !!}</td>';
    cols += '<td width="3%"><a class="ibtnDel btn btn-md btn-danger"><i class="fa fa-minus"></i></a>';    
    newRow.append(cols);
    $("table.order-list").append(newRow);
    $('#row'+counter+'').find('.new').select2({
      placeholder: "Seleccione",
      tags:true,
      createTag: function (params) {
        return {
          id: params.term,
          text: params.term,
          newOption: true }
        },
      templateResult: function (data) {
        var $result = $("<span></span>");
        $result.text(data.text);
        if (data.newOption) {
          $result.append(" <em>(Nuevo)</em>");
        }
        return $result;
      },
      width:'100%',
      language: "es",
    }).val(null).trigger('change');

    $('#'+counter+'').on({
      "focus": function(event) {
        $(event.target).select();
      },
      "keyup": function(event) {
        $(event.target).val(function(index, value) {
          return value.replace(/\D/g, "")
            .replace(/([0-9])([0-9]{2})$/, '$1,$2')
            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
        });
      }
    });
    counter++;
});

$("#addrow").on("click", function () {
    var newRow = $("<tr>");
    var cols = "";
    cols += '<td>{!! Form::text('beneficiario[]', null, ['class' => 'form-control', 'placeholder'=>'Nombre del Beneficiario', 'required' => 'required']) !!}</td>';
    cols += '<td>{!! Form::text('concepto[]', null, ['class' => 'form-control', 'placeholder'=>'Concepto de la operación', 'required' => 'required']) !!}</td>';
    cols += '<td>{!! Form::text('justificacion[]', null, ['class' => 'form-control', 'placeholder'=>'Justificación de la operación', 'required' => 'required']) !!}</td>';
    cols += '<td width="15%"><input type="text" class="form-control numero" placeholder="----" required="true" id="'+counter+'" name="monto[]" dir="rtl" onkeypress="return soloNumeros(event)"></td>';
    cols += '<td width="10%">{!! Form::date('fecha_tope[]', null, ['class' => 'form-control', 'placeholder'=>'Fecha tope de pago']) !!}</td>';
    cols += '<td width="8%">{!! Form::text('nro_orden[]', null, ['class' => 'form-control', 'placeholder'=>'----','onkeypress'=>'return soloNumeros(event)']) !!}</td>';
    cols += '<td width="8%"> {!! Form::text('nro_factura[]', null, ['class' => 'form-control', 'placeholder'=>'----', 'onkeypress'=>'return soloNumeros(event)']) !!}</td>';
    cols += '<td width="3%"><a class="ibtnDel btn btn-md btn-danger"><i class="fa fa-minus"></i></a>';
    newRow.append(cols);
    $("table.order-list").append(newRow);
    $('#'+counter+'').on({
      "focus": function(event) {
        $(event.target).select();
      },
      "keyup": function(event) {
        $(event.target).val(function(index, value) {
          return value.replace(/\D/g, "")
            .replace(/([0-9])([0-9]{2})$/, '$1,$2')
            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
        });
      }
    });
    counter++;
});

$("table.order-list").on("click", ".ibtnDel", function (event) {
    $(this).closest("tr").remove();       
    counter -= 1
});

$(".numero").on({
  "focus": function(event) {
    $(event.target).select();
  },
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/\D/g, "")
        .replace(/([0-9])([0-9]{2})$/, '$1,$2')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
    });
  }
});
  
$(document).ready(function(){
moment.updateLocale('es', {
  week: { dow: 0 } // Monday is the first day of the week
});

var currentTime = new Date() ;

if (currentTime.getDay() == 0 || currentTime.getDay() == 1 @if(Auth::user()->rol_id == 4) || currentTime.getDay() == 2 @endif ) {
  var first = currentTime.getDate() - currentTime.getDay() - 0; // First day is the day of the month - the day of the week
  first = new Date(currentTime.setDate(first));
  }
else {
  if (currentTime.getDay() == 2) {
    var first = currentTime.setDate(currentTime.getDate() + 5);
  }
  if (currentTime.getDay() == 3) {
    var first = currentTime.setDate(currentTime.getDate() + 4);
  }
  if (currentTime.getDay() == 4) {
    var first = currentTime.setDate(currentTime.getDate() + 3);

  }
  if (currentTime.getDay() == 5) {
    var first = currentTime.setDate(currentTime.getDate() + 2);
  }
  if (currentTime.getDay() == 6) {
    var first = currentTime.setDate(currentTime.getDate() + 1);
  } 
}


  //Initialize the datePicker(I have taken format as mm-dd-yyyy, you can     //have your owh)
  $("#semana").datetimepicker({
      format: 'DD-MM-YYYY',
      locale: 'es',
      minDate: first,
  });


});

function soloNumeros(e){
  key = e.keyCode || e.which;
  tecla = String.fromCharCode(key).toLowerCase();
  letras = "1234567890";
  especiales = "8-37-39-46";

  tecla_especial = false
  for(var i in especiales){
    if(key == especiales[i]){
      tecla_especial = true;
      break;
    }
  }

  if(letras.indexOf(tecla)==-1 && !tecla_especial){
    return false;
  }
}

   //Get the value of Start and End of Week
  $('#semana').on('dp.change', function (e) {
      var value = $("#semana").val();
      var firstDate = moment(value, "DD-MM-YYYY").day(0).format("DD-MM-YYYY");
      var lastDate =  moment(value, "DD-MM-YYYY").day(6).format("DD-MM-YYYY");
      $("#semana").val(firstDate + " AL " + lastDate);
  });
</script>

@endsection