@extends('templates.master')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/prueba/buttons.dataTables.min.css')}}">

@endsection

@section('titulo_modulo', Auth::user()->nombre.' - Bienvenid@')

@section('contenido')
<div class="row" >

  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
          <a class="btn btn-default" href="{{route('cuentas_tesoreria.create')}} ">
            <span class="text-success"><i class="fa fa-plus"></i>Agregar Cuentas</span>
          </a>
          <br /><br />
          <table id="example1" class="table table-bordered table-striped " style="text-align: center; font-size: smaller;">
            <thead>
              <tr>
                <th>ID</th>
                <th style="text-align: center;">BENEFICIARIO</th>
                <th style="text-align: center;">CONCEPTO</th>
                <th style="text-align: center;">JUSTIFICACIÓN</th>
                <th style="text-align: center;">SEMANA DEL</th>
                <th style="text-align: center;">FECHA TOPE</th>
                <th style="text-align: center;">NRO. ORDEN DE COMPRA</th>
                <th style="text-align: center;">NRO. FACTURA</th>
                <th style="text-align: center;">ESTATUS</th>
                <th style="text-align: center;">OBSERVACIÓN</th>
                <th style="text-align: center;">MONTO PROGRAMADO</th>
                <th style="text-align: center;">MONTO PAGADO</th>
                <th style="text-align: center;">VARIACIÓN</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach($cuentas as $cuenta)
              @if($cuenta->departamento_id == Auth::user()->id)
              <tr>
                <td>{{ $cuenta->id }}</td>
                <td>{{ $cuenta->beneficiario}}</td>
                <td>{{ $cuenta->concepto }}</td>
                <td>{{ $cuenta->justificacion }}</td>
                <td>{{ $cuenta->semana }}</td>
                <td>{{ $cuenta->fecha_tope }}</td>
                <td>{{ $cuenta->nro_orden }}</td>
                <td>{{ $cuenta->nro_factura }}</td>
                  @if($cuenta->estatus=='POR APROBAR' || $cuenta->estatus=='POR PRE-APROBAR' ) 
                    <td class="text-danger">
                      {{ $cuenta->estatus }}
                    </td>
                  @elseif ($cuenta->estatus=='APROBADO' || $cuenta->estatus=='PRE-APROBADO')
                    <td class="text-info">
                      {{ $cuenta->estatus }}
                    </td>
                  @elseif ($cuenta->estatus=='PAGADO')
                    <td class="text-success" >
                      {{ $cuenta->estatus }}  
                    </td>
                  @elseif ($cuenta->estatus=='RECHAZADO')
                    <td class="text-muted" >
                      {{ $cuenta->estatus }}</br>
                    </td>                      
                  @endif
                <td>{{ $cuenta->observacion }}</td>
                <td>{{ $cuenta->monto_programado }}</td>
                <td>{{ $cuenta->monto_pagado }}</td>
                <td>{{ $cuenta->variacion }}</td>
                <td>
                  @if(  ( Auth::user()->rol_id == 4 && $cuenta->estatus == 'POR PRE-APROBAR' && $editable_especial=='SI') || ((($esta_semana==$cuenta->semana  && $editable=='SI')|| $semana2==$cuenta->semana || $semana3==$cuenta->semana || $semana4==$cuenta->semana ) && $cuenta->estatus == 'POR APROBAR' ) ) 
                    <div class="btn-group" style="width: 50px">
                      <button type="button" class="btn btn-warning btn-xs" title="Editar" OnClick="showCuenta({{ $cuenta->id }})">
                        <i class="fa fa-edit"></i>
                      </button>
                      <button type="button" class="btn btn-danger btn-xs"title="Eliminar" style="margin-left:2.5px;" OnClick="deleteCuenta({{ $cuenta->id }})">
                        <i class="fa fa-remove"></i>
                      </button>
                    </div>
                  @endif
                </td>
              </tr>
              @endif
              @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>ID</th>
                <th style="text-align: center;">BENEFICIARIO</th>
                <th style="text-align: center;">CONCEPTO</th>
                <th style="text-align: center;">JUSTIFICACIÓN</th>
                <th style="text-align: center;">SEMANA DEL</th>
                <th style="text-align: center;">FECHA TOPE</th>
                <th style="text-align: center;">NRO. ORDEN DE COMPRA</th>
                <th style="text-align: center;">NRO. FACTURA</th>
                <th style="text-align: center;">ESTATUS</th>
                <th style="text-align: center;">OBSERVACIÓN</th>
                <th style="text-align: center;">MONTO PROGRAMADO</th>
                <th style="text-align: center;">MONTO PAGADO</th>
                <th style="text-align: center;">VARIACIÓN</th>
                <th></th>
              </tr>
            </tfoot>
          </table>
        </table>
        
      </div><!-- /.box-body -->
  </div>
</div>
@include('dashboard.Depto.cuenta_modal')  
@endsection

@section('js')
<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/jszip.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/buttons.html5.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/dataTables.colReorder.min.js')}}"></script>

<script type="text/javascript" src="{{asset('public/js/departamentos.js')}}"></script>

<script type="text/javascript">
$(".numero").on({
  "focus": function(event) {
    $(event.target).select();
  },
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/\D/g, "")
        .replace(/([0-9])([0-9]{2})$/, '$1,$2')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
    });
  }
});

function soloNumeros(e){
  key = e.keyCode || e.which;
  tecla = String.fromCharCode(key).toLowerCase();
  letras = "1234567890";
  especiales = "8-37-39-46";

  tecla_especial = false
  for(var i in especiales){
    if(key == especiales[i]){
      tecla_especial = true;
      break;
    }
  }

  if(letras.indexOf(tecla)==-1 && !tecla_especial){
    return false;
  }
}

</script>

@endsection