@extends('templates.master')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">

@endsection

@section('titulo_modulo','Realizar pago')

@section('contenido')
<div class="row" >

  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
          <div class="col-md-12">
            {!! Form::open(['method' => 'PUT', 'route' => 'cuentas_tesoreria.guardar_pago', 'class' => 'form-horizontal']) !!}
            <table id="myTable" class=" table order-list text-center">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>AREA</th>
                    	<th>BENEFICIARIO</th>
                        <th>CONCEPTO</th>
                        <th>SEMANA</th>
                        <th>MONTO PROGRAMADO</th>
                        <th>MONTO A PAGAR</th>
                    </tr>
                </thead>
                <tbody>
                	@foreach($cuentas as $cuenta)
                    <tr>
                        <td >{{ $cuenta->id}} </td>
                        <td >{{ $cuenta->usuario->nombre}} </td>
                        <td >{{ $cuenta->beneficiario}}</td>
                        <td >{{ $cuenta->concepto }}</td>
                        <td >{{ $cuenta->semana }}</td>
                        <td >{{ number_format($cuenta->monto_programado,2,',','.') }}</td>
                        <td>
                        	{!! Form::hidden('id[]', $cuenta->id) !!}
                          <input type="text" class="form-control numero" placeholder="----" required="true" name="monto[]" dir="rtl" onkeypress="return soloNumeros(event)">
                        </td>
                    </tr>
					@endforeach
				</tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>AREA</th>
                        <th>BENEFICIARIO</th>
                        <th>CONCEPTO</th>
                        <th>SEMANA</th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
            <div class="col-md-12">
                <br />
                  {!! Form::reset("Resetear", ['class' => 'btn btn-warning']) !!} 
                  <a href="{{ URL::previous() }}" class="btn btn-info">Cancelar</a>
                  <input type="submit" value="Guardar" class='btn btn-success pull-right' onclick="return confirm('¿Está seguro que quiere guardar estos registros? Nota: No se podrá modificar ni eliminar una vez guardados.');" >
            </div>
            {!! Form::close() !!}
          </div>
      </div>
</div>
  
@endsection

@section('js')
<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script type="text/javascript">
$('#myTable').DataTable({
    language: leng,
    order: [[ 0, "desc" ]],
    "autoWidth": true,
    "columnDefs": [{ "orderable": false, "targets": [4] }],
    initComplete: function () {
      this.api().columns([0,1,2,4]).every( function () {
          var column = this;
          var select = $('<select><option value=""></option></select>')
              .appendTo( $(column.footer()).empty() )
              .on( 'change', function () {
                  var val = $.fn.dataTable.util.escapeRegex(
                      $(this).val()
                  );

                  column
                      .search( val ? '^'+val+'$' : '', true, false )
                      .draw();
              } );

          column.data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' )
          } );
      } );
    }
});

$(".numero").on({
  "focus": function(event) {
    $(event.target).select();
  },
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/\D/g, "")
        .replace(/([0-9])([0-9]{2})$/, '$1,$2')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
    });
  }
});

function soloNumeros(e){
  key = e.keyCode || e.which;
  tecla = String.fromCharCode(key).toLowerCase();
  letras = "1234567890";
  especiales = "8-37-39-46";

  tecla_especial = false
  for(var i in especiales){
    if(key == especiales[i]){
      tecla_especial = true;
      break;
    }
  }

  if(letras.indexOf(tecla)==-1 && !tecla_especial){
    return false;
  }
}

</script>

@endsection