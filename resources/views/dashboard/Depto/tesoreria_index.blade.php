@extends('templates.master')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/prueba/buttons.dataTables.min.css')}}">

<style type="text/css">
table.dataTable tbody td { 
  vertical-align: middle; 
}
table.dataTable thead th { 
  vertical-align: middle; 
}
</style>

@endsection

@section('titulo_modulo', Auth::user()->nombre.' - Bienvenid@')

@section('contenido')
<div class="row" >

  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
          <a class="btn btn-default btn-flat" href="{{route('cuentas_tesoreria.create')}} ">
            <span class="text-success"><i class="fa fa-plus"></i>Agregar Cuentas</span>
          </a>
          <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-tabla"><i class="fa fa-repeat"></i> Actualizar</a>
          <br /><br />
          <table id="example1" class="table table-bordered table-hover table-striped compact" style="text-align: center; font-size: smaller;">
            <thead >
              <tr style="text-align: center;">
                <th>
                  <input name="select_all" value="1" id="select-all-cuentas" type="checkbox">
                </th>
                <th style="text-align: center;">ID</th>
                <th style="text-align: center;">SEMANA DEL</th> 
                <th style="text-align: center;">AREAS</th>
                <th style="text-align: center;">BENEFICIARIOS</th> 
                <th style="text-align: center;">CONCEPTOS</th>
                <th style="text-align: center;">FECHA TOPE</th>
                <th style="text-align: center;">NRO. ORDEN DE COMPRA</th>
                <th style="text-align: center;">NRO. FACTURA</th>
                <th style="text-align: center;">ESTATUS</th>  
                <th style="text-align: center;">MONTOS PROGRAMADOS</th>
                <th style="text-align: center;">IGTF PROGRAMADOS</th> 
                <th style="text-align: center;">TOTAL PROGRAMADOS</th> 
                <th style="text-align: center;">MONTO PAGADOS</th> 
                <th style="text-align: center;">IGTF PAGADOS</th> 
                <th style="text-align: center;">TOTAL PAGADOS</th> 
                <th style="text-align: center;">VARIACIÓN</th> 
                <th></th> 
              </tr>
            </thead>

            <tfoot>
              <tr>
                <th></th>
                <th style="text-align: center;">ID</th>
                <th style="text-align: center;">SEMANA DEL</th> 
                <th style="text-align: center;">AREAS</th>
                <th style="text-align: center;">BENEFICIARIOS</th> 
                <th style="text-align: center;">CONCEPTOS</th>
                <th style="text-align: center;">FECHA TOPE</th>
                <th style="text-align: center;">NRO. ORDEN DE COMPRA</th>
                <th style="text-align: center;">NRO. FACTURA</th>
                <th style="text-align: center;">ESTATUS</th>  
                <th style="text-align: center;">MONTOS PROGRAMADOS</th>
                <th style="text-align: center;">IGTF PROGRAMADOS</th> 
                <th style="text-align: center;">TOTAL PROGRAMADOS</th> 
                <th style="text-align: center;">MONTO PAGADOS</th> 
                <th style="text-align: center;">IGTF PAGADOS</th> 
                <th style="text-align: center;">TOTAL PAGADOS</th> 
                <th style="text-align: center;">VARIACIÓN</th> 
                <th></th>  
              </tr>
            </tfoot>
          </table>
      </div>
      <div class="box-footer">
        <button class="btn btn-info btn-sm btn-flat" style="width: 100px"  id="btn-aprobar-select"><i class="fa fa-check"></i> APROBAR</button>
        <button class="btn btn-danger btn-sm btn-flat" style="width: 100px" id="btn-rechazar-select"><i class="fa fa-close"></i> RECHAZAR</button>
        <a class="btn btn-success btn-sm btn-flat pull-right" style="width: 100px" href="{{route('cuentas_tesoreria.por_pagar')}} ">
          <i class="fa fa-money"></i> PAGAR
        </a>
      </div><!-- /.box-footer-->
    </div>
  </div>
</div>

@include('dashboard.Depto.cuenta_modal')
  
@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/jszip.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/buttons.html5.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/dataTables.colReorder.min.js')}}"></script>

<script type="text/javascript" src="{{asset('public/js/tesoreria.js')}}"></script>

<script type="text/javascript">
$(".numero").on({
  "focus": function(event) {
    $(event.target).select();
  },
  "keyup": function(event) {
    $(event.target).val(function(index, value) {
      return value.replace(/\D/g, "")
        .replace(/([0-9])([0-9]{2})$/, '$1,$2')
        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
    });
  }
});
</script>

@endsection