@extends('templates.master')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/prueba/buttons.dataTables.min.css')}}">

<style type="text/css">
table.dataTable tbody td { 
  vertical-align: middle; 
}
table.dataTable thead th { 
  vertical-align: middle; 
}
</style>

@endsection

@section('titulo_modulo', Auth::user()->nombre.' - Bienvenid@')

@section('contenido')
<div class="row" >

  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
          <a href='javascript:void(0)' class="btn btn-default pull-right"  id="actualizar-tabla"><i class="fa fa-repeat"></i> Actualizar</a>
          <br /><br />
        <table id="example1" class="table table-bordered table-hover table-striped compact" style="text-align: center; font-size: smaller;">
          <thead >
            <tr style="text-align: center;">
              <th style="text-align: center;">ID</th>
              <th style="text-align: center;">SEMANA DEL</th> 
              <th style="text-align: center;">AREAS</th>
              <th style="text-align: center;">BENEFICIARIOS</th> 
              <th style="text-align: center;">CONCEPTOS</th>
              <th style="text-align: center;">JUSTIFICACIÓN</th>
              <th style="text-align: center;">FECHA TOPE</th>
              <th style="text-align: center;">NRO. ORDEN DE COMPRA</th>
              <th style="text-align: center;">NRO. FACTURA</th>
              <th style="text-align: center;">ESTATUS</th>  
              <th style="text-align: center;">MONTOS PROGRAMADOS</th>
              <th style="text-align: center;">IGTF PROGRAMADOS</th> 
              <th style="text-align: center;">TOTAL PROGRAMADOS</th> 
              <th style="text-align: center;">MONTO PAGADOS</th> 
              <th style="text-align: center;">IGTF PAGADOS</th> 
              <th style="text-align: center;">TOTAL PAGADOS</th> 
              <th style="text-align: center;">VARIACIÓN</th>  
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th style="text-align: center;">ID</th>
              <th style="text-align: center;">SEMANA DEL</th> 
              <th style="text-align: center;">AREAS</th>
              <th style="text-align: center;">BENEFICIARIOS</th> 
              <th style="text-align: center;">CONCEPTOS</th>
              <th style="text-align: center;">JUSTIFICACIÓN</th>
              <th style="text-align: center;">FECHA TOPE</th>
              <th style="text-align: center;">NRO. ORDEN DE COMPRA</th>
              <th style="text-align: center;">NRO. FACTURA</th>
              <th style="text-align: center;">ESTATUS</th>  
              <th style="text-align: center;">MONTOS PROGRAMADOS</th>
              <th style="text-align: center;">IGTF PROGRAMADOS</th> 
              <th style="text-align: center;">TOTAL PROGRAMADOS</th> 
              <th style="text-align: center;">MONTO PAGADOS</th> 
              <th style="text-align: center;">IGTF PAGADOS</th> 
              <th style="text-align: center;">TOTAL PAGADOS</th> 
              <th style="text-align: center;">VARIACIÓN</th> 
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
  
@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/jszip.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/buttons.html5.min.js')}}"></script>

<script type="text/javascript">
  var ruta_cuenta = ruta+'/cerradas';
  $.fn.dataTable.ext.errMode = 'throw';

  var tabla =$('#example1').DataTable({
      dom: 
        "<'row'<'col-sm-1'B><'col-sm-2 text-center'l><'col-sm-9'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      processing: true,
      serverSide: true,
      ajax: ruta_cuenta+'/listar',
      search: { "caseInsensitive": true },
      columns: [
      { data: 'id', name: 'id'},
      { data: 'semana', name: 'semana'},
      { data: 'area', name: 'area'},
      { data: 'beneficiario', name: 'beneficiario'},
      { data: 'concepto', name: 'concepto'},
      { data: 'justificacion', name: 'justificacion'},
      { data: 'fecha_tope', name: 'fecha_tope'},
      { data: 'nro_orden', name: 'nro_orden'},
      { data: 'nro_factura', name: 'nro_factura'},
      { data: 'estatus', name: 'estatus', 
      render: function(data, type, full, meta){
        var text;
        if(data == 'RECHAZADO'){
          text= '<span class="text-danger">'+data+'</span></br>'+full.observacion;      
        }
        else{
          text = '<span class="text-success">'+data+'</span>';
        }
      return text;
      }},
      { data: 'monto_programado', name: 'monto_programado'},
      { data: 'igtf_programado', name: 'igtf_programado'},
      { data: 'total_programado', name: 'total_programado'},
      { data: 'monto_pagado', name: 'monto_pagado'},
      { data: 'igtf_pagado', name: 'igtf_pagado'},
      { data: 'total_pagado', name: 'total_pagado'},
      { data: 'variacion', name: 'variacion'},
      ],      

      language: leng,
      order: [[ 0, "desc" ]],
      "bAutoWidth": false,
      "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
      iDisplayLength: -1,
      "scrollX": true,
      initComplete: function () {
        this.api().columns([2,3,9]).every( function () {
            var column = this;
            var select = $('<select><option value=""></option></select>')
                .appendTo( $(column.footer()).empty() )
                .on( 'change', function () {
                    var val = $.fn.dataTable.util.escapeRegex(
                        $(this).val()
                    );

                    column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                } );

            column.data().unique().sort().each( function ( d, j ) {
                select.append( '<option value="'+d+'">'+d+'</option>' )
            } );
        } );
      },
      "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
      buttons: [
            {
              extend: 'excelHtml5',
              exportOptions: {
                  columns: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],
                  search: 'applied',
                  order: 'applied' //column id visible in PDF 
                } 
            },
        ],
  })

    $('#actualizar-tabla').click(function(){
      tabla.ajax.reload();
    })
</script>

@endsection