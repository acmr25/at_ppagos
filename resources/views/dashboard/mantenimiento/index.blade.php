@extends('templates.master')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/prueba/buttons.dataTables.min.css')}}">

<!-- sweealert -->
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/sweealert/sweetalert2.min.css')}}">

@endsection

@section('titulo_modulo', 'Bienvenid@')

@section('contenido')
<div class="row">
	<div class="col-sm-6">
		@include('dashboard.roles.index')
	</div>
	<div class="col-sm-6">
		@include('dashboard.usuarios.index')
	</div>
</div>
@endsection

@section('js')
<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/jszip.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/buttons.html5.min.js')}}"></script>

<!-- sweealert -->
<script type="text/javascript" src="{{asset('public/plugins/sweealert/promise.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/plugins/sweealert/sweetalert2.min.js')}}"></script>

<script type="text/javascript">
function starLoad(btn){
	$(btn).button('loading');
	$('.load-ajax').addClass('overlay');
	$('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
} 

function endLoad(btn){
	$(btn).button('reset');
	$('.load-ajax').removeClass('overlay');
	$('.load-ajax').fadeIn(1000).html("");
} 
</script>

<script type="text/javascript" src="{{asset('public/js/roles.js')}}"></script>

<script type="text/javascript">
  $.fn.dataTable.ext.errMode = 'throw';

var tabla_usuarios = $("#tabla-usuarios").DataTable({
  processing: true,
  serverSide: true,
  ajax: ruta+'/usuarios/listar',
  search: { "caseInsensitive": true },
  columns: [
  { data: 'id', name: 'id'},
  { data: 'nombre', name: 'nombre'},
  { data: 'rol_id', name: 'rol_id'},
  { data: 'estado', name: 'estado'},
  { data: 'id',
    'orderable': false,
    render: function ( data, type, full, meta ) {
      var text = '<div class="text-center ">'+
      '<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" OnClick="UsuarioActual('+data+')"><i class="fa fa-edit"></i></button>'+
      '<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar" style="margin-left:2.5px;" OnClick="deleteUsuario(' + data + ')"><i class="fa fa-remove"></i></button>'+
      '</div>'; 
      return text;
    }
  }],
  order: [[ 0, "desc" ]],
  scrollY:  "500px",
  scrollCollapse: true,
  language: leng,
  "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "TODOS"]],
});

$('#actualizar-user').click(function(){
  tabla_usuarios.ajax.reload();
})

function deleteUsuario(id){
  swal({
    title: '¿Estás seguro que quiere eliminar?',
    text: "Esta acción no podra ser revertida!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Si, eliminar',
    cancelButtonText: 'No, cancelar',
    showLoaderOnConfirm: true,
    preConfirm: function() {
      return new Promise(function(resolve, reject) {
        var route = ruta+'/usuarios/'+id;
        $.ajax({
          url: route,
          type: 'DELETE',
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          success: function(res){ 
            resolve()
            tabla_usuarios.ajax.reload();
          },
          error: function(jqXHR, textStatus, errorThrown) {
            swal(
              'Error',
              'Ha ocurrido un error al tratar de eliminar al empleado. Status: '+jqXHR.status,
              'error'
              )
          }
        })
      });
    },
    allowOutsideClick: false
  }).then(function() {
    swal(
      'Eliminado!',
      'El Usuario se ha eliminado exitosamente',
      'success'
      );
  });
}

$(document).ready(function() {

    $.getJSON(ruta+"/roles/getRoles", function(jsonData){
        select = '<select name="rol_id" class="form-control" required id="rol_id" placeholder="Seleccione">';
        select +='<option value="">Seleccione</option>';
          $.each(jsonData, function(i,data)
          {
            select +='<option value="'+data.id+'">'+data.nombre+'</option>';
           });
        select += '</select>';
        $("#rol_id").html(select);
	});
});

function mayus(e) {
    e.value = e.value.toUpperCase();
}

</script>

@endsection