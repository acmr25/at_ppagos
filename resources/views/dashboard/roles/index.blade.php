<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">Roles</h3>
	</div>
	<div class="box-body">
		<a class="btn btn-default btn-sm"  data-toggle="modal" href='#modal-rol'> <span class="text-success"><i class="fa fa-plus"></i></span> Crear nuevo rol</a>
		<a href='javascript:void(0)' class="btn btn-default btn-sm pull-right"  id="actualizar-roles"><i class="fa fa-repeat"></i> Actualizar tabla</a>
		<hr>
		<table class="table table-bordered table-hover table-striped" id="tabla-roles" width="100%">
			<thead>
				<th style="width: 30px;">ID</th>
				<th>Nombre</th>
				<th>Descripcion</th>
				<th style="width: 50px;">Acción</th>
			</thead>
		</table>
	</div><!-- /.box-body -->
</div><!-- /.box -->

@include('dashboard.roles.form')