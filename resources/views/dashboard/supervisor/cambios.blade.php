@extends('templates.master')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">

@endsection

@section('titulo_modulo','Realizar pago')

@section('contenido')
<div class="row" >

  <div class="col-md-12">
    <div class="box box-success">
      <div class="box-body">
          <div class="col-md-12">
            {!! Form::open(['method' => 'PUT', 'route' => 'supervisor.procesado', 'class' => 'form-horizontal']) !!}
            <table id="myTable" class=" table order-list text-center">
                <thead>
                    <tr>
                      <th>ID</th>
                      <th>AREA</th>
                    	<th>BENEFICIARIO</th>
                      <th>CONCEPTO</th>
                      <th>JUSTIFICACIÓN</th>
                      <th>SEMANA</th>
                      <th>MONTO PROGRAMADO</th>
                      <th>RESPUESTA</th>
                    </tr>
                </thead>
                <tbody>
                  @php
                    $nose=0;
                  @endphp
                	@foreach($cuentas as $cuenta)
                    <tr>
                      <td >{{ $cuenta->id}} </td>
                      <td >{{ $cuenta->usuario->nombre}} </td>
                      <td >{{ $cuenta->beneficiario}}</td>
                      <td >{{ $cuenta->concepto }}</td>
                      <td >{{ $cuenta->justificacion }}</td>
                      <td >{{ $cuenta->semana }}</td>
                      <td >{{ number_format($cuenta->monto_programado,2,',','.') }}</td>
                      <td>
                      	{!! Form::hidden('id[]', $cuenta->id) !!}
                        <div class="input-group">
                          <span class="input-group-addon">
                            <input type="radio" name="clase[{{$cuenta->id}}]" value="PRE-APROBADO" onclick="deshabilitar('{{ $cuenta->id}}')" required="true">Pre-Aprobar
                            <input type="radio" name="clase[{{$cuenta->id}}]" value="RECHAZADO"  onclick="habilitar('{{ $cuenta->id}}')">Rechazar
                          </span>
                          {!! Form::textarea('observacion['.$cuenta->id.']', null, ['id' =>$cuenta->id, 'class' => 'form-control','rows'=>1, 'disabled'=>true]) !!}
                        </div>
                      </td>
                    </tr>
                      @php
                        $nose++;
                      @endphp
                  @endforeach
				        </tbody>
                <tfoot>
                    <tr>
                      <th>ID</th>
                      <th>AREA</th>
                      <th>BENEFICIARIO</th>
                      <th>CONCEPTO</th>
                      <th>JUSTIFICACIÓN</th>
                      <th>SEMANA</th>
                      <th>MONTO PROGRAMADO</th>
                      <th>RESPUESTA</th>
                    </tr>
                </tfoot>
            </table>
            <div class="col-md-12">
                <br />
                  {!! Form::reset("Resetear", ['class' => 'btn btn-warning']) !!} 
                  <a href="{{ URL::previous() }}" class="btn btn-info">Cancelar</a>
                  <input type="submit" value="Guardar" class='btn btn-success pull-right' onclick="return confirm('¿Está seguro que quiere guardar estos registros? Nota: No se podrá modificar ni eliminar una vez guardados.');" >
            </div>
            {!! Form::close() !!}
          </div>
      </div>
</div>
  
@endsection

@section('js')
<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<script type="text/javascript">
$('#myTable').DataTable({
    language: leng,
    order: [[ 0, "asc" ]],
    "autoWidth": true,
    "columnDefs": [{ "orderable": false, "targets": [6] }],
    initComplete: function () {
      this.api().columns([0,1,2,5]).every( function () {
          var column = this;
          var select = $('<select><option value=""></option></select>')
              .appendTo( $(column.footer()).empty() )
              .on( 'change', function () {
                  var val = $.fn.dataTable.util.escapeRegex(
                      $(this).val()
                  );

                  column
                      .search( val ? '^'+val+'$' : '', true, false )
                      .draw();
              } );

          column.data().unique().sort().each( function ( d, j ) {
              select.append( '<option value="'+d+'">'+d+'</option>' )
          } );
      } );
    }
});

function habilitar(id){
  document.getElementById(id).required = true;
  document.getElementById(id).disabled = false;
  document.getElementById(id).focus();
}
function deshabilitar(id){
  document.getElementById(id).required = false;
  document.getElementById(id).disabled = true;
}
</script>

@endsection