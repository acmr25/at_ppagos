@extends('templates.master')

@section('css')

<!-- DataTables -->  
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/dataTables.bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/plugins/datatables/prueba/buttons.dataTables.min.css')}}">

<style type="text/css">
table.dataTable tbody td { 
  vertical-align: middle; 
}
table.dataTable thead th { 
  vertical-align: middle; 
}
</style>

@endsection

@section('titulo_modulo', Auth::user()->nombre.' - Bienvenid@')

@section('contenido')
<div class="row" >

  <div class="col-md-12">
    <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">Pagos Programados para Pre-Aprobar</h3>
          <div class="box-tools">
            <a href='javascript:void(0)' class="btn btn-default btn-sm btn-flat"  id="actualizar-tabla"><i class="fa fa-repeat"></i> Actualizar</a>
            <a class="btn btn-warning btn-sm btn-flat" href="{{route('supervisor.procesar')}} ">
              <i class="fa fa-check"></i><i class="fa fa-close"></i> PRE-APROBAR/RECHAZAR
            </a>
          </div>
        </div>
      <div class="box-body">
        <table id="example1" class="table table-bordered table-hover table-striped compact" style="text-align: center; font-size: smaller;">
          <thead >
            <tr style="text-align: center;">
              <th style="text-align: center;">ID</th>
              <th style="text-align: center;">SEMANA DEL</th> 
              <th style="text-align: center;">AREA</th>
              <th style="text-align: center;">BENEFICIARIO</th> 
              <th style="text-align: center;">CONCEPTO</th>
              <th style="text-align: center;">JUSTIFICACIÓN</th>
              <th style="text-align: center;">FECHA TOPE</th>
              <th style="text-align: center;">NRO. ORDEN DE COMPRA</th>
              <th style="text-align: center;">NRO. FACTURA</th>
              <th style="text-align: center;">MONTO PROGRAMADO</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th style="text-align: center;">ID</th>
              <th style="text-align: center;">SEMANA DEL</th> 
              <th style="text-align: center;">AREA</th>
              <th style="text-align: center;">BENEFICIARIO</th> 
              <th style="text-align: center;">CONCEPTO</th>
              <th style="text-align: center;">JUSTIFICACIÓN</th>
              <th style="text-align: center;">FECHA TOPE</th>
              <th style="text-align: center;">NRO. ORDEN DE COMPRA</th>
              <th style="text-align: center;">NRO. FACTURA</th>
              <th style="text-align: center;">MONTO PROGRAMADO</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
  
@endsection

@section('js')

<!-- DataTables -->
<script src="{{asset('public/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/jszip.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/buttons.html5.min.js')}}"></script>
<script src="{{asset('public/plugins/datatables/prueba/dataTables.colReorder.min.js')}}"></script>

<script type="text/javascript" src="{{asset('public/js/supervisor.js')}}"></script>


@endsection