<!-- Modal -->
<div class="modal fade" id="modal-usuario" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content box">
      <div class="modal-header text-center">
        {!! Form::open(['id'=>'form-usuario']) !!}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Formulario para Usuarios</h4>
      </div>
      <div class="modal-body">
        {!! Form::hidden('token', csrf_token(), ['id'=>'token']) !!}
        {!! Form::hidden('id', null, ['id'=>'usuario_id']) !!} 
        <div class=" form-group" id="field-nombre">
          {!! Form::label('nombre', 'Nombre del Departamento/Area:') !!}
          {!! Form::text('nombre', null, ['id' => 'nombre','class' => 'form-control', 'required' => 'required', 'placeholder' => 'Cuentas por pagar','onkeypress'=>'mayus(this);']) !!}
          <span><strong class="text-danger msj-error"></strong></span>   
        </div>
        <div class="row">
          <div class="form-group col-md-6" id="field-password">
              {!! Form::label('password', 'Contraseña:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                {!! Form::password('password', ['id' => 'password','class' => 'form-control', 'required' => 'required', 'placeholder'=>'********']) !!}
              </div>
              <span><strong class="text-danger msj-error"></strong></span>
          </div>        
          <div class="form-group col-md-6" id="field-password_confirmation">
              {!! Form::label('password_confirmation', 'Confirme Contraseña:') !!}
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-unlock-alt" style="font-size: 17px"></i></span>
                {!! Form::password('password_confirmation', ['id' => 'password_confirmation','class' => 'form-control', 'required' => 'required', 'placeholder'=>'********']) !!}
              </div>
              <span><strong class="text-danger msj-error"></strong></span>
          </div>           
        </div>
        @if(Auth::user()->rol_id == 3)
        <div class="row">
          <div class="form-group col-md-6" id="CAMPO_ROL">
              {!! Form::label('rol_id','Tipo de Usuario') !!}
              {!! Form::select('rol_id',[], null, ['id' => 'rol_id', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
          <div class="form-group col-md-6" id="CAMPO_ESTADO">
              {!! Form::label('estado', 'Estatus de Usuarios') !!}
              {!! Form::select('estado', ['ACTIVO'=>'ACTIVO','INACTIVO'=>'INACTIVO'], null, ['id' => 'estado', 'class' => 'form-control', 'required' => 'required', 'placeholder'=>'Seleccione']) !!}
              <span><strong class="text-danger msj-error"></strong></span>
          </div>
        </div>
        @else
          {!! Form::hidden('rol_id', null, ['id'=>'rol_id']) !!}
          {!! Form::hidden('estado', null, ['id'=>'estado']) !!}
        @endif
      </div>

      <div class="modal-footer">
        <div >
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
          <button type="button" id="guardar-usuario" class="btn btn-success pull-right" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
        </div>      
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
