<div class="box box-solid">
	<div class="box-header with-border">
		<h3 class="box-title">Usuarios / Areas</h3>
	</div>
	<div class="box-body">
		<button type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#modal-usuario">
            <span class="text-success"><i class="fa fa-plus"></i></span> Agregar Usuario
        </button>
		<a href='javascript:void(0)' class="btn btn-default btn-sm pull-right"   id="actualizar-user"><i class="fa fa-repeat"></i> Actualizar tabla</a>
		<hr>
		<table class="table table-bordered table-hover table-striped" id="tabla-usuarios" width="100%">
			<thead>
				<th style="width: 30px;">ID</th>
				<th>Nombre</th>
				<th>Rol</th>
				<th>Estatus</th>
				<th style="width: 50px;">Acción</th>
			</thead>
		</table>
	</div><!-- /.box-body -->
</div><!-- /.box -->

@include('dashboard.usuarios.form')