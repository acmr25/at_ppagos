<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>@yield('title', 'Programación de Pagos AT')</title>

    <link rel="stylesheet" href="{{ asset('public/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/plugins/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/plugins/Ionicons/css/ionicons.min.css') }}">
    <!-- sweealert -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/plugins/sweealert/sweetalert2.min.css')}}">
    
    @yield('css')

    <link rel="stylesheet" href="{{ asset('public/plugins/adminlte/css/AdminLTE.min.css') }}">

    <link rel="stylesheet" href="{{ asset('public/plugins/adminlte/css/skins/_all-skins.min.css') }}">


</head>
<body class="hold-transition skin-green layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Logo -->
        <a href="{{ url('') }}" class="logo pull-left">
          <!-- logo for regular state and mobile devices -->
        <span class="logo-lg" style="color: white;"><b>AMAZONAS TECH</b></span> 
        </a>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            @if(Auth::user()->rol_id == 3)
              <li @if(isset($li) && $li=='mantenimiento') class="active" @endif> <a href="{{ url('/mantenimiento') }}">Mantenimiento</a></li>
            @endif
            @if(Auth::user()->rol_id == 2 || Auth::user()->rol_id == 5)
              <li @if(isset($li) && $li=='cerradas') class="active" @endif> <a href="{{ url('/cerradas') }}">Registros Cerrados</a></li>
            @endif
          </ul>  
          </form>
        </div>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                 <span class="glyphicon glyphicon-user"></span><span class="caret"></span></a>
              <ul style="font-size: 18px" class="dropdown-menu">
                <li><a  data-toggle="tooltip" data-placement="top" title="Editar" OnClick="UsuarioActual({{ Auth::user()->id }})"><i class="fa fa-user text-aqua"></i>Perfil</a></li>
                <li><a href="{{ url('/logout') }}"><i class="fa fa-power-off text-danger"></i>Salir</a></li>
              </ul>
            </li>
          </ul>
        </div>
    </nav>
  </header>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>@yield('titulo_modulo')</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      @include('dashboard.usuarios.form')
      @include('flash::message')
      @yield('contenido')

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1
    </div>
    <strong>Copyright &copy; 2017-2018 <a href="#">Amazonas Tech C.A</a>.</strong>
  </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('public/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('public/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('public/plugins/adminlte/js/adminlte.min.js') }}"></script>

<!-- sweealert -->
<script type="text/javascript" src="{{asset('public/plugins/sweealert/promise.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/plugins/sweealert/sweetalert2.min.js')}}"></script>

<script type="text/javascript">
  //FLASH TIEMPO DE VISTA
  $('div.alert').not('.alert-important').delay(4000).fadeOut(1250);
  //LENGUAJE ESPAÑOL PARA LAS DATATABLES
  var leng = {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
    "sFirst":    "Primero",
    "sLast":     "Último",
    "sNext":     "Siguiente",
    "sPrevious": "Anterior"
    },
    "oAria": {
    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
  };
  var ruta = "{{ url('') }}";
</script>

<script src="{{asset('public/js/usuario.js')}}"></script>

@yield('js')


</body>
</html>